package src.homework.homework_21.task_7;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        int[] arr1 = new int[N];
        for (int i = 0; i < N; i++){
            arr1[i] = scan.nextInt();
        }
        int[] arr2 = new int[N];
        for (int i = 0; i < N; i++){
            arr2[i] = arr1[i] * arr1[i];
        }
        Arrays.sort(arr2);
        for (int i: arr2)
            System.out.print(i + " ");
    }
}