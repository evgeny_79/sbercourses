package src.homework.homework_21.task_4;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int len = scan.nextInt();
        int[] array = new int[len];
        int i;
        for (i = 0; i < len; i++){
            array[i] = scan.nextInt();
        }
        int count = 0;
        int element;
        for (i = 0; i < len; ){
            element = array[i];
            for (int j = i; j < len; j++){
                if (element == array[j]) {
                    count++;
                    i++;
                }
                else {                
                    break;
                }
            }
            System.out.println(count + " " + element);
            count = 0;
        }
    }
}