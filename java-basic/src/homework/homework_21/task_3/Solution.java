package src.homework.homework_21.task_3;

import java.util.Scanner;
public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int len = scan.nextInt();
        int[] array = new int[len];
        for (int i = 0; i < len; i++){
            array[i] = scan.nextInt();
        }
        int num = scan.nextInt();
        int index = 0;
        for (int i = 0; i < len - 1; i++){
            if (num == array[i]) {
                if (num == array[i + 1])
                    continue;
                index = i + 1;
                break;
            }
            else if (num < array[i]) {
                index = i;
                break;
            }
            else {
                index = i + 1;
            }


        }        
        System.out.println(index);

    }
}