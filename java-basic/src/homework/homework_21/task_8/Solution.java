package src.homework.homework_21.task_8;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        int[] arr = new int[N];
        for (int i = 0; i < N; i++){
            arr[i] = scan.nextInt();
        }
        int M = scan.nextInt();
        int element = arr[0];
        int index = 0;
        for (int i = 0; i < N; i++){
            if (Math.abs(arr[i] - M) < Math.abs(element - M) || Math.abs(arr[i] - M) == Math.abs(element - M) && arr[i] > element){
                element = arr[i];

            }
        }
        System.out.println(element);
    }
}