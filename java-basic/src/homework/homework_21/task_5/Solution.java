package src.homework.homework_21.task_5;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int len = scan.nextInt();
        int[] array1 = new int[len];
        for (int i = 0; i < len; i++){
            array1[i] = scan.nextInt();
        }
        int M = scan.nextInt();
        int[] array2 = new int[len];
        for (int i = 0; i < len; i++){
            if (i + M < len)
                array2[i + M] = array1[i];
            else
                array2[i + M - len] = array1[i];
        }
        for (int i: array2)
            System.out.print(i + " ");
    }
}