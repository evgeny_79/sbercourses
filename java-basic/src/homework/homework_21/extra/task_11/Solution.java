package src.homework.homework_21.extra.task_11;

import java.util.Scanner;
import java.util.Random;

public class Solution {
    private static final int MIN_LENGTH = 8;
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N;
        do {
            System.out.print("Введите длину пароля (не менее 8-ми символов): ");
            N = scan.nextInt();
            if (N < 8)
                System.out.printf("Пароль с %d количеством символов небезопасен.%n", N);
        } while (N < MIN_LENGTH);
        System.out.println(genPassword(N));

    }

    private static String genPassword(int N){
        Random random = new Random();
        String[] strings = {"1234567890",
                            "_-*",
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                            "abcdefghijklmnopqrstuvwxyz"};
        StringBuilder stringBuilder = new StringBuilder();
        int rand;
        do {
            for (int i = 0; i < N; i++) {
                rand = random.nextInt(4);
                stringBuilder.append(strings[rand].charAt(random.nextInt(strings[rand].length())));
            }
        } while (!checkPassword(stringBuilder));
        return stringBuilder.toString();
    }
    private static boolean checkPassword(StringBuilder str){
        return str.toString().matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_*-])[^\\s]{8,}$");
    }
}