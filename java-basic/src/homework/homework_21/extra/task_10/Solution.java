package src.homework.homework_21.extra.task_10;

import java.util.Scanner;
import java.util.Random;

public class Solution {
    private static final int MAX_VALUE = 1000;
    public static void main(String[] args) {
        startGame();
    }
    private static void startGame(){
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int userNumber;
        int hiddenNumber = random.nextInt(MAX_VALUE + 1);
        System.out.printf("Компьютер загадал число от 0 до %d. Попробуйте угадать это число.", MAX_VALUE);
        while (true){
            System.out.print("Введите число: ");
            userNumber = scanner.nextInt();
            if (userNumber == hiddenNumber) {
                System.out.println("Победа!");
                break;
            }
            else if (userNumber > hiddenNumber)
                System.out.println("Это число больше загаданного.");
            else
                System.out.println("Это число меньше загаданного.");
        }
    }
}