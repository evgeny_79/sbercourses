package src.homework.homework_21.task_9;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        scan.nextLine();
        String[] strings = new String[N];
        for (int i = 0; i < N; i++){
            strings[i] = scan.nextLine();
        }
        int i;
        boolean duplicate = false;
        for (i = 0; (i < N - 1) & !duplicate; i++){
            for (int j = i + 1; j < N; j++){
                if (strings[i].equals(strings[j])) {
                    duplicate = true;
                    break;
                }
            }
        }
        System.out.println(strings[i - 1]);
    }
}