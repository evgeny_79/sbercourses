package src.homework.homework_21.task_2;

import java.util.Scanner;
import java.util.Arrays;
public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int len_1 = scan.nextInt();
        int[] array_1 = new int[len_1];
        for (int i = 0; i < len_1; i++){
            array_1[i] = scan.nextInt();
        }
        int len_2 = scan.nextInt();
        int[] array_2 = new int[len_2];
        for (int i = 0; i < len_2; i++){
            array_2[i] = scan.nextInt();
        }
        System.out.println((len_1 == len_2 && Arrays.equals(array_1, array_2)) ? true : false);
    }
}