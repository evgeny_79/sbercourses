package src.homework.homework_21.task_1;

import java.util.Scanner;
public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int len = scan.nextInt();
        double sum = 0;
        double[] array = new double[len];
        for (int i = 0; i < len; i++){
            array[i] = scan.nextDouble();
            sum += array[i];
        }
        System.out.println(sum / len);
    }
}