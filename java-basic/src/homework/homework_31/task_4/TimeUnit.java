package src.homework.homework_31.task_4;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeUnit {
    private int hour;
    private int minute;
    private int second;

    public TimeUnit(int hour, int minute, int second) {
        this.hour = setHour(hour);
        this.minute = setMinutes(minute);
        this.second = setSeconds(second);
    }

    public TimeUnit(int hour, int minute) {
        this.hour = setHour(hour);
        this.minute = setMinutes(minute);
        this.second = 0;
    }

    public TimeUnit(int hour) {
        this.hour = setHour(hour);
        this.minute = 0;
        this.second = 0;
    }

    public int getHour() {
        return hour;
    }

    private int setHour(int hour) {
        if (hour < 0 || hour > 23) {
            throw new IllegalArgumentException("Неверный аргумент hour");
        }
        return hour;
    }

    private int setMinutes(int minute) {
        if (minute < 0 || minute > 59) {
            throw new IllegalArgumentException("Неверный аргумент minute");
        }
        return minute;
    }
    private int setSeconds(int second) {
        if (second < 0 || second > 23) {
            throw new IllegalArgumentException("Неверный аргумент second");
        }
        return second;
    }
    public void showTime24h(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(" 'Установленное время: ' HH:mm:ss");
        System.out.println(dtf.format(LocalTime.of(hour, minute, second)));
    }
    public void showTime12h(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(" 'Установленное время 12-часовом формате: ' hh:mm:ss a");
        System.out.println(dtf.format(LocalTime.of(hour, minute, second)));
    }

    // Метод, который прибавляет переданное время к установленному
    public void setTime(int h, int m, int s){
        LocalTime lt = LocalTime.of(this.hour, this.minute, this.second);
        this.hour = lt.plusHours(h).getHour();
        this.minute = lt.plusMinutes(m).getMinute();
        this.second = lt.plusSeconds(s).getSecond();
    }
}
