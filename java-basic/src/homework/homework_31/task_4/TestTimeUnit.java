package src.homework.homework_31.task_4;

import java.time.LocalTime;

public class TestTimeUnit {
    public static void main(String[] args) {
        TimeUnit tu = new TimeUnit(23);

        tu.showTime24h();
        tu.showTime12h();

        tu.setTime(22, 45, 45);

        tu.showTime24h();
        tu.showTime12h();
    }
}
