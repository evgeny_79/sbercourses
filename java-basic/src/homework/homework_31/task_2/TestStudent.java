package src.homework.homework_31.task_2;

import java.util.Arrays;

public class TestStudent {
    public static void main(String[] args) {
        Student student_1 = new Student("Ivan", "Ivanov");
        for (int i = 1; i <= 11; i++)
            student_1.addGrade(i);
        System.out.println(Arrays.toString(student_1.getGrades()));
        System.out.println(student_1.averageOfGrades()); 
    }
}
