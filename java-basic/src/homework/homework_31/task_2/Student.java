package src.homework.homework_31.task_2;

public class Student {
    private String name;
    private String surname;
    private int[] grades;
    private final int maxCountOfGrades = 10;
    private int indexOfGrades;

    public Student(String name, String surname){
        this.name = name;
        this.surname = surname;
        grades = new int[maxCountOfGrades];
        indexOfGrades = 0;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int grade){
        grades[indexOfGrades++] = grade;
    }

    public int[] getGrades() {
        return grades;
    }

    public void addGrade(int grade){
        if (indexOfGrades == maxCountOfGrades){
            int[] newGrades = new int[maxCountOfGrades];
            System.arraycopy(grades, 1, newGrades, 0, grades.length - 1);
            grades = newGrades;
            indexOfGrades--;
        }
        setGrades(grade);
    }

    public double averageOfGrades(){
        int sum = 0;
        for (int i = 0; i < indexOfGrades; i++)
            sum += grades[i];
        return sum == 0 ? 0: (double) sum / indexOfGrades;
    }
}
