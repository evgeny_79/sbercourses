package src.homework.homework_31.task_8;

public class TestAtm {
    public static void main(String[] args) {
        Atm atm = new Atm(0.0146, 68.71);
        System.out.printf("%.2f rub = %.2f usd%n", 1000.0, atm.convertRubInDol(1000));
        System.out.printf("%.2f usd = %.2f rub%n", 1000.0, atm.convertDolInRub(1000));

    }
}
