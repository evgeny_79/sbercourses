package src.homework.homework_31.task_8;

public class Atm {
    private static int count = 0;
    private double rubInUsd;
    private double usdInRub;

    public Atm(double rubInUsd, double usdInRub) {
        this.rubInUsd = rubInUsd;
        this.usdInRub = usdInRub;
        count++;
    }
    public double convertRubInDol(double rub){
        return rub * rubInUsd;
    }
    public double convertDolInRub(double usd){
        return usd * usdInRub;
    }
    public static int getCount(){
        return count;
    }
}
