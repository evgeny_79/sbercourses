package src.homework.homework_31.task_6;

public class AmazingString {
    private char[] chars;

    public AmazingString(char[] chars) {
        this.chars = chars;
    }
    public AmazingString(String str) {
        this.chars = str.toCharArray();
    }
    // Вернуть i-ый символ строки
    public char getChar(int index){
        return chars[index];
    }
    // Вернуть длину строки
    public int getLengthString(){
        return chars.length;
    }
    // Вывести строку на экран
    public void showString(){
        for (char ch: chars)
            System.out.print(ch);
        System.out.println();
    }
    /* Проверить, есть ли переданная подстрока в AmazingString
    * (на вход подается массив char) */
    public boolean contains(char[] ch){
        if (ch.length > chars.length) {
            return false;
        }
        for (int i = 0; i <= chars.length - ch.length; i++) {
            boolean result = true;
            for (int j = 0; j < ch.length; j++) {
                if (chars[i + j] != ch[j]) {
                    result = false;
                    break;
                }
            }
            if (result) {
                return true;
            }
        }
        return false;
    }
    /* Проверить, есть ли переданная подстрока в AmazingString,
     (на вход подается String).  */
    public boolean contains(String str){
        return contains(str.toCharArray());
    }
    /* Удалить из строки AmazingString ведущие пробельные символы,
     если они есть*/
    public void stripSpace() {
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != ' ') {
                if (i == 0)
                    break;
                char[] newChars = new char[chars.length - i];
                System.arraycopy(chars, i, newChars, 0, newChars.length);
                chars = newChars;
                break;
            }
        }
    }
    // Развернуть строку
    public void reverse(){
        char temp;
        for (int i = 0, j = chars.length - 1; i < j; i++, j--){
            temp = chars[i];
            chars[i] = chars[j];
            chars[j] = temp;
        }
    }
}
