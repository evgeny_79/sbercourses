package src.homework.homework_31.task_6;

public class TestAmazingString {
    public static void main(String[] args) {
        AmazingString obj_1 = new AmazingString("  Hello");
        System.out.printf("4-символ строки: %c%n", obj_1.getChar(4));
        System.out.printf("Длина строки: %d%n", obj_1.getLengthString());
        obj_1.showString();
        System.out.printf("Содержит подстроку 'lo' : %b%n", obj_1.contains(new char[]{'l', 'o'}));
        System.out.printf("Содержит подстроку 'lo' : %b%n", obj_1.contains("lo"));
        System.out.printf("Содержит подстроку 'elo' : %b%n", obj_1.contains("elo"));
        obj_1.showString();
        obj_1.stripSpace(); // удаляем ведущие пробелы
        obj_1.showString();
        obj_1.reverse(); // разворачиваем строку
        obj_1.showString();
    }
}
