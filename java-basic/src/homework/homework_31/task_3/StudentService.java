package src.homework.homework_31.task_3;

public class StudentService {
    public void bestStudent(Student[] students){
        try {
            Student bestStudent = students[0];
            for (int i = 1; i < students.length; i++){
                if (students[i].averageOfGrades() > bestStudent.averageOfGrades())
                    bestStudent = students[i];
            }
            System.out.println("Лучший студент: " + bestStudent);
        }
        catch (NullPointerException e){
            System.out.println("Некорректный список");
        }

    }

    public void sortByName(Student[] students){
        try {
            Student tempStudent;
            for (int i = 1; i < students.length; i++){
                for (int j = 0; j < students.length - i; j++){
                    if (students[j].getSurname().compareToIgnoreCase(students[j + 1].getSurname()) > 0){
                        tempStudent = students[j];
                        students[j] = students[j + 1];
                        students[j + 1] = tempStudent;
                    }
                }
            }
        } catch (NullPointerException e) {
            System.out.println("Некорректный список");
        }
    }
}
