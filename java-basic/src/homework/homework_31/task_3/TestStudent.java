package src.homework.homework_31.task_3;

public class TestStudent {
    public static void main(String[] args) {
        StudentService studentService = new StudentService();

        Student[] students = new Student[5];
        students[0] = new Student("Иван", "Иванов", new int[]{1,2,3,4,5,6,7,8,9,10,11});
        students[1] = new Student("Петр", "Петров", new int[]{1,2,3});
        students[2] = new Student("Алексей", "Алексеев", new int[]{1,2,3,4,5,6,7,8,9,10,11,12});
        students[3] = new Student("Олег", "Олегов", new int[]{1,2,3,4,5,6});
        students[4] = new Student("Борис", "Борисов", new int[]{1,2,3,4,5,6,7,8});

        studentService.bestStudent(students);

        System.out.println("Список студентов:");
        for (Student student: students)
            System.out.println("   " + student);

        studentService.sortByName(students);
        System.out.println("Список студентов (отсортированный):");
        for (Student student: students)
            System.out.println("   " + student);
    }
}
