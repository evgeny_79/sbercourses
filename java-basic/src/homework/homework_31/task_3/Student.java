package src.homework.homework_31.task_3;
public class Student {
    private String name;
    private String surname;
    private int[] grades;
    private final int maxCountOfGrades = 10;
    private int indexOfGrades;

    public Student(String name, String surname){
        this.name = name;
        this.surname = surname;
        grades = new int[maxCountOfGrades];
        indexOfGrades = 0;
    }
    public Student(String name, String surname, int[] array){
        this.name = name;
        this.surname = surname;
        this.grades = checkArray(array);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int grade){
        grades[indexOfGrades++] = grade;
    }

    public int[] getGrades() {
        return grades;
    }

    // метод, добавляющий новую оценку в grades
    public void addGrade(int grade){
        if (indexOfGrades == maxCountOfGrades){
            int[] newGrades = new int[maxCountOfGrades];
            System.arraycopy(grades, 1, newGrades, 0, grades.length - 1);
            grades = newGrades;
            indexOfGrades--;
        }
        setGrades(grade);
    }

    /* метод проверяющий размер массива
    * оценок студента */
    public int[] checkArray(int[] array){
        if (array.length > maxCountOfGrades){
            int[] newGrades = new int[maxCountOfGrades];
            System.arraycopy(array, array.length - maxCountOfGrades, newGrades, 0, maxCountOfGrades);
            indexOfGrades = maxCountOfGrades;
            return newGrades;
        }
        return array;
    }

    // метод, возвращающий средний балл студента
    public double averageOfGrades(){
        int sum = 0;
        for (int i = 0; i < indexOfGrades; i++)
            sum += grades[i];
        return sum == 0 ? 0: (double) sum / indexOfGrades;
    }

    public String toString(){
        return name + " " + surname;
    }
}
