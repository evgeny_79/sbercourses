package src.homework.homework_31.task_5;

public class TestDayOfWeek {
    public static void main(String[] args) {
        DayOfWeek[] days = new DayOfWeek[]{
                new DayOfWeek((byte) 1, "Monday"),
                new DayOfWeek((byte) 2, "Tuesday"),
                new DayOfWeek((byte) 3, "Wednesday"),
                new DayOfWeek((byte) 4, "Thursday"),
                new DayOfWeek((byte) 5, "Friday"),
                new DayOfWeek((byte) 6, "Saturday"),
                new DayOfWeek((byte) 7, "Sunday"),
        };

        for (DayOfWeek day: days){
            System.out.printf("%d %s%n", day.getIndex(), day.getDayOfWeek());
        }

    }
}
