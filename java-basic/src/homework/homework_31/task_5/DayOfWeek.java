package src.homework.homework_31.task_5;

public class DayOfWeek {
    private byte index;
    private String dayOfWeek;

    public DayOfWeek(byte index, String dayOfWeek) {
        this.index = index;
        this.dayOfWeek = dayOfWeek;
    }

    public byte getIndex() {
        return index;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }
}
