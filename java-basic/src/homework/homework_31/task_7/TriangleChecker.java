package src.homework.homework_31.task_7;

public class TriangleChecker {
    static boolean isTriangle(double a, double b, double c){
        if ((a + b > c) && (a + c > b) && (b + c > a))
            return true;
        return false;
    }
}
