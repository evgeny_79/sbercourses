package src.homework.homework_22.task_1;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); // кол-во столбцов
        int M = scanner.nextInt(); // кол-во строк
        int[][] arr = new int[M][N];
        for (int i = 0; i < M; i++){
            for (int j = 0; j < N; j++){
                arr[i][j] = scanner.nextInt();
            }
        }
        int[] minNumbers = new int[M];
        int min;
        for (int i = 0; i < M; i++){
            min = arr[i][0];
            for (int j = 1; j < N; j++){
                if (arr[i][j] < min){
                    min = arr[i][j];
                }
            }
            minNumbers[i] = min;
        }
        for (int i: minNumbers){
            System.out.print(i + " ");
        }
    }
}