package src.homework.homework_22.task_6;

import java.util.Scanner;
public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int k = sc.nextInt();

        int[] arr = new int[4];

        for (int i = 0; i < 7; i++) {
            arr[0] += sc.nextInt();
            arr[1] += sc.nextInt();
            arr[2] += sc.nextInt();
            arr[3] += sc.nextInt();
        }

        if (
            arr[0] < a &&
            arr[1] < b &&
            arr[2] < c &&
            arr[3] < k
        ) {
            System.out.println("Отлично");
        } else {
            System.out.println("Нужно есть поменьше");
        }
    }
}