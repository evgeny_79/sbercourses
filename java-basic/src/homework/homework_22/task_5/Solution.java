package src.homework.homework_22.task_5;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int[][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = sc.nextInt();
            }
        }

        boolean result = true;

        for (int i = 0; i < n; i++) {
            if (!result) {
                break;
            } else {
                for (int j = 0; j < n; j++) {
                    if (arr[i][j] != arr[n - 1 - j][n - 1 - i]) {
                        result = false;
                        break;
                    }
                }
            }
        }

        System.out.println(result);
    }
}