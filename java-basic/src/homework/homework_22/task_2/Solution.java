package src.homework.homework_22.task_2;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); // размер матрицы
        int x1 = scanner.nextInt(); // столбец 1 
        int y1 = scanner.nextInt(); // строка 1 
        int x2 = scanner.nextInt(); // столбец 2 
        int y2 = scanner.nextInt(); // строка 2
        for (int row = 0; row < N; row++){
            for (int column = 0; column < N; column++){
                if (row == y1 || row == y2){ // верхняя или нижняя сторона прям-ка
                    if (column >= x1 && column <= x2) 
                        System.out.print(1);
                    else
                        System.out.print(0);
                }
                else if (row > y1 && row < y2){  // середина прям-ка
                    if (column == x1 || column == x2)
                        System.out.print(1);
                    else
                        System.out.print(0);
                }
                else
                    System.out.print(0);
                if (column != N - 1) // после последнего элемента строки пробел не ставим
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
}