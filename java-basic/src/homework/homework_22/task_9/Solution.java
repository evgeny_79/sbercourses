package src.homework.homework_22.task_9;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        meth(N);
    }
    public static void meth(int n){
        if (n < 10)
            System.out.print(n + " ");
        else {
            meth(n / 10);
            System.out.print(n % 10 + " ");
        }
    }
}