package src.homework.homework_22.task_8;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        System.out.println(meth(N));
    }
    public static int meth(int n){
        if (n < 10)
            return n;
        else {
            return n % 10 + meth(n / 10);
        }
    }
}