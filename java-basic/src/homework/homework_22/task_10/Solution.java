package src.homework.homework_22.task_10;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        meth(N);

    }
    public static void meth(int n){
        if (n > 0){
            System.out.print(n % 10 + " ");
            meth(n / 10);
        }
    }
}