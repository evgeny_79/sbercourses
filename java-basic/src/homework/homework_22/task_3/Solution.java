package src.homework.homework_22.task_3;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int x = sc.nextInt();
        int y = sc.nextInt();

        char[][] arr = new char[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = '0';
            }
        }

        insertK(arr, x, y);

        insertX(arr, n, x, y, -2, -1);
        insertX(arr, n, x, y, -1, -2);
        insertX(arr, n, x, y, 1, -2);
        insertX(arr, n, x, y, 2, -1);
        insertX(arr, n, x, y, 2, 1);
        insertX(arr, n, x, y, 1, 2);
        insertX(arr, n, x, y, -1, 2);
        insertX(arr, n, x, y, -2, 1);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j]);
                if (j < n - 1) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void insertK(char[][] arr, int x, int y) {
        arr[y][x] = 'K';
    }

    private static void insertX(char[][] arr, int n, int x, int y, int offsetX, int offsetY) {
        if (
            x + offsetX < 0 ||
            x + offsetX > n - 1 ||
            y + offsetY < 0 ||
            y + offsetY > n - 1
        ) {
        } else {
            arr[y + offsetY][x + offsetX] = 'X';
        }
    }
}