package src.homework.homework_22.task_7;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        String[] people = new String[N];
        String[] animals = new String[N];
        double[][] rating = new double[N][4];

        // имена хозяев животных
        for (int i = 0; i < people.length; i++){
            people[i] = scanner.next();
        }
        // клички животных
        for (int i = 0; i < animals.length; i++){
            animals[i] = scanner.next();
        }
        // Оценки 3-х судей для каждой строки
        for (int i = 0; i < rating.length; i++){
            for (int j = 0; j < rating[0].length - 1; j++){
                rating[i][j] = scanner.nextDouble();
            }
        }
        // Находим среднее арифметическое оценок судей
        for (int i = 0; i < rating.length; i++){
            for (int j = 0; j < rating[0].length; j++){
                rating[i][3] =  (rating[i][0]
                               + rating[i][1]
                               + rating[i][2]) / 3;
            }
        }

        int indexLargest = 0;
        int indexSecondLargest = 0;
        int indexThirdLargest = 0;
        for (int i = 1; i < rating.length; i++){
            if (rating[i][3] > rating[indexLargest][3]){
                indexSecondLargest = indexLargest;
                indexLargest = i;
            }
            else if (rating[i][3] > rating[indexSecondLargest][3]){
                indexThirdLargest = indexSecondLargest;
                indexSecondLargest = i;
            }
            else if (rating[i][3] > rating[indexThirdLargest][3]){
                indexThirdLargest = i;
            }
        }
        DecimalFormat df = new DecimalFormat("0.0");
        df.setRoundingMode(RoundingMode.DOWN);
        System.out.printf("%s: %s, %s%n", people[indexLargest],
                                         animals[indexLargest],
                                         df.format(rating[indexLargest][3]));
        System.out.printf("%s: %s, %s%n", people[indexSecondLargest],
                                         animals[indexSecondLargest],
                                         df.format(rating[indexSecondLargest][3]));
        System.out.printf("%s: %s, %s%n", people[indexThirdLargest],
                                         animals[indexThirdLargest],
                                         df.format(rating[indexThirdLargest][3]));

    }
}