package src.homework.homework_32;

public class Book {
    private String bookName;
    private String bookAuthor;
    private double bookRating;
    private boolean isBookOccupied;
    private int countOfReaders;
    private int userIDTakingTheBook;

    public Book(String name, String author) {
        this.bookName = name;
        this.bookAuthor = author;
        this.bookRating = 0;
        this.isBookOccupied = false;
        this.countOfReaders = 0;
    }

    public String getBookName() {
        return bookName;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public double getBookRating() {
        return bookRating;
    }
    public void setBookRating(double bookRating) {
        this.bookRating += bookRating;
    }

    public boolean getBookOccupied() { return isBookOccupied; }
    public void setBookOccupied(boolean bookOccupied) {
        isBookOccupied = bookOccupied;
    }

    public int getUserIDTakingTheBook() {
        return userIDTakingTheBook;
    }
    public void setUserIDTakingTheBook(int userIDTakingTheBook) {
        this.userIDTakingTheBook = userIDTakingTheBook;
    }

    public int getCountOfReaders() {
        return countOfReaders;
    }
    public void setCountOfReaders() {
        this.countOfReaders++;
    }

    public String toString(){
        return String.format("книга - \"%s\"", getBookName());
    }
}
