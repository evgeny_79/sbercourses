package src.homework.homework_32;

import java.util.ArrayList;

public class Library {
    private ArrayList<Book> library;
    private static int userID = 0;

    public Library(){
        library = new ArrayList<>();
    }

    public ArrayList<Book> getLibrary() {
        return library;
    }

    // 1) добавить книгу
    public void addBook(Book book){
        if (searchBookByName(book.getBookName()) == -1){
            library.add(book);
            System.out.println("Книга добавлена.");
        }
        else {
            System.out.println("Эта книга уже есть в списке книг библиотеки.");
        }
    }

    // 2) удаление книги по названию
    public void removeBook(String str){
        int index = searchBookByName(str);
        // если книга в списке и не занята
        if (index >= 0 && !library.get(index).getBookOccupied()) {
            library.remove(index);
            System.out.println("Книга удалена.");
        }
        else if (index >= 0 && library.get(index).getBookOccupied()){
            System.out.println("Удаление невозможно. Книга занята.");
        }
        else {
            System.out.println("Удаление невозможно. Такой книги нет в списке книг библиотеки.");
        }
    }

    // 3) поиск книги по названию
    public int searchBookByName(String str) {
        for (int i = 0; i < library.size(); i++){
            if (library.get(i).getBookName().equals(str)){
                return i;
            }
        }
        return -1;
    }

    // 4) Найти и вернуть список книг по автору
    public ArrayList searchBookByAuthor(String str){
        ArrayList<Book> lst = new ArrayList<>();
        for (Book b: library){
            if (b.getBookAuthor().equals(str)){
                lst.add(b);
            }
        }
        return lst;
    }

    // отображение библиотеки
    public void displayLibrary(ArrayList lst){
        for (int i = 0; i < lst.size(); i++){
            System.out.printf("книга: %s%n", lst.get(i).toString());
        }
    }

    // поиск ID пользователя, взявшего книгу
    public int searchUserID(User user){
        for (int i = 0; i < library.size(); i++){
            if (user.getId() == library.get(i).getUserIDTakingTheBook()){
                return i;
            }
        }
        return -1;
    }

    // 5) выдать книгу
    public void getBook(String str, User user){
        /* если пользователь в первый раз берет книгу
        * или не имеет книг на руках */
        int index1 = searchUserID(user);
        if (user.getId() == 0 || index1 == -1) {
            int index2 = searchBookByName(str);
            // если книга в списке и не занята
            if (index2 >= 0 && !library.get(index2).getBookOccupied()) {
                if (user.getId() == 0) {
                    /* присваиваем пользователю ID
                    * в первый раз взявшему книгу */
                    user.setId(++userID);
                }
                // отмечаем, что книга занята
                library.get(index2).setBookOccupied(true);
                // сохраняем ID пользователя, взявшего книгу
                library.get(index2).setUserIDTakingTheBook(user.getId());
                // увеличиваем счетчик пользователей, взявших эту книгу
                library.get(index2).setCountOfReaders();
                System.out.printf("%s выдана%n", library.get(index2).toString());
            } else if (library.get(index2).getBookOccupied()) {
                System.out.println(library.get(index2).toString() + " занята.");
            } else {
                System.out.println("Такой книги нет в списке книг библиотеки.");
            }
        }
        else {
            System.out.printf("Пользователь с таким ID взял: %s%n", library.get(index1).toString());
        }
    }

    // 6) вернуть книгу
    public void returnBook(User user, double raiting){
        int index = searchUserID(user);
        if (index >= 0){
            // ставим оценку книге
            library.get(index).setBookRating(raiting);
            // отмечаем, что книга свободна
            library.get(index).setBookOccupied(false);
            // удаляем ID пользователя, взявшего книгу
            library.get(index).setUserIDTakingTheBook(0);
            System.out.printf("Пользователь с таким ID сдал: %s%n", library.get(index).toString());
        }
        else {
            System.out.printf("Пользователь с таким ID не брал книг%n");
        }
    }

    // 8) оценка книги
    public void computeRatingBook(String str){
        int index = searchBookByName(str);
        if (index >= 0){
            if (library.get(index).getCountOfReaders() > 0 && library.get(index).getBookRating() > 0){
                System.out.printf("Рейтинг книги \"%s\" - %.1f%n", library.get(index).getBookName(),
                        library.get(index).getBookRating() / library.get(index).getCountOfReaders());
            }
            else
                System.out.printf("Рейтинг книги \"%s\" - %d%n", library.get(index).getBookName(), 0);
        }
        else
            System.out.println("Такой книги нет в списке книг библиотеки.");
    }

}
