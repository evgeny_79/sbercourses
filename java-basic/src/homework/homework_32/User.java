package src.homework.homework_32;

public class User {
    private String name;
    private int id;

    public User(String name){
        this.name = name;
        this.id = 0;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
