package src.homework.homework_32;

public class Test {
    public static void main(String[] args) {
        Library l = new Library();
        l.addBook(new Book("Преступление и наказание", "Ф.М.Достоевский"));
        l.addBook(new Book("Война и мир", "Л.Н.Толстой"));
        l.addBook(new Book("Анна Каренина", "Л.Н.Толстой"));
        l.addBook(new Book("Java - полное руководство", "Г.Шилдт"));
        // добавим дубликат
        l.addBook(new Book("Java - полное руководство", "Г.Шилдт"));

        User user1 = new User("Evgeny");
        User user2 = new User("Anatoliy");
        User user3 = new User("Dmitriy");

        // Найти и вернуть список книг по автору
        l.displayLibrary(l.searchBookByAuthor("Л.Н.Толстой"));

        l.getBook("Война и мир", user1);
        // пробуем удалить взятую книгу
        l.removeBook("Война и мир");
        // даем пользователю еще книгу
        l.getBook("Java - полное руководство", user1);
        // даем другому пользователю взятую книгу
        l.getBook("Война и мир", user2);
        //l.displayLibrary();
        // возвращаем книгу
        l.returnBook(user1, 4.5);
        // даем другому пользователю книгу
        l.getBook("Война и мир", user3);
        // возвращаем книгу
        l.returnBook(user3, 5);
        // рейтинг книги
        l.computeRatingBook("Война и мир");

        l.removeBook("Война и мир");
        //l.displayLibrary();
        l.displayLibrary(l.getLibrary());


    }
}
