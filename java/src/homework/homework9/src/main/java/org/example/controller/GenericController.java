package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.dto.GenericDTO;
import org.example.mapper.GenericMapper;
import org.example.models.GenericModel;
import org.example.service.GenericService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public abstract class GenericController <T extends GenericModel, N extends GenericDTO>{

    private final GenericService<T> genericService;
    private final GenericMapper<T, N> mapper;

    protected GenericController(GenericService<T> genericService, GenericMapper<T, N> mapper) {
        this.genericService = genericService;
        this.mapper = mapper;
    }

    @Operation(description = "Получить запись по ID", method = "getById")
    @GetMapping(value = "/getById", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> getById(@RequestParam Long id) {
        return ResponseEntity.ok(mapper.toDTO(genericService.getOne(id)));
    }

    @Operation(description = "Получить все записи", method = "getAll")
    @GetMapping(value = "/getAll", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<N>> getAll() {
        return ResponseEntity.ok(mapper.toDTOs(genericService.listAll()));
    }

    @Operation(description = "Создать новую запись", method = "create")
    @PostMapping(value = "/add", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<N> create(@RequestBody N newEntity) {
        if (newEntity.getId() != null && genericService.existsById(newEntity.getId())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapper.toDTO(genericService.create(mapper.toEntity(newEntity))));
    }

    @Operation(description = "Обновить запись", method = "update")
    @PutMapping(value = "/update", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<N> update(@RequestBody N updatedEntity,
                                    @RequestParam Long id) {
        updatedEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapper.toDTO(genericService.update(mapper.toEntity(updatedEntity))));
    }

    @Operation(description = "Удалить запись по ID", method = "delete")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        genericService.delete(id);
    }
}
