package org.example.mapper;

import java.util.List;

public interface Mapper<E, D> {

    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);

    D toDTO(E entity);

    List<D> toDTOs(List<E> entities);
}
