package org.example.service;

import org.example.models.Film;
import org.example.repository.FilmRepository;
import org.springframework.stereotype.Service;

@Service
public class FilmService extends GenericService<Film>{

    private final FilmRepository filmRepository;

    public FilmService(FilmRepository filmRepository) {
        super(filmRepository);
        this.filmRepository = filmRepository;
    }
}
