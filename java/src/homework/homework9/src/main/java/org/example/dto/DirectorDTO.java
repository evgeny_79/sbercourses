package org.example.dto;

import java.util.Set;

public class DirectorDTO extends GenericDTO{
    private String directorFIO;
    private int position;
    private Set<Long> filmsID;

    public DirectorDTO() {
    }

    public String getDirectorFIO() {
        return directorFIO;
    }

    public void setDirectorFIO(String directorFIO) {
        this.directorFIO = directorFIO;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Set<Long> getFilmsID() {
        return filmsID;
    }

    public void setFilmsID(Set<Long> filmsID) {
        this.filmsID = filmsID;
    }
}
