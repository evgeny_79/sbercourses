package org.example.service;

import org.example.models.User;
import org.example.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User>{

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }
}
