package org.example.dto;

public class AddFilmDTO {

    private Long filmID;
    private Long directorID;

    public AddFilmDTO() {
    }

    public Long getFilmID() {
        return filmID;
    }

    public void setFilmID(Long filmID) {
        this.filmID = filmID;
    }

    public Long getDirectorID() {
        return directorID;
    }

    public void setDirectorID(Long directorID) {
        this.directorID = directorID;
    }
}
