package org.example.models;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "films")
public class Film extends GenericModel{

    @Column(name = "title")
    private String title;

    @Column(name = "premier_year")
    private LocalDate premierYear;

    @Column(name = "country")
    private String country;

    @Enumerated(EnumType.STRING)
    private Genre genre;

    /* по умолчанию Lazy */
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "film_director",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "director_id")
    )
    private Set<Director> directors = new HashSet<>();

    public Film() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getPremierYear() {
        return premierYear;
    }

    public void setPremierYear(LocalDate premierYear) {
        this.premierYear = premierYear;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Set<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(Set<Director> directors) {
        this.directors = directors;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", premierYear=" + premierYear +
                ", country='" + country + '\'' +
                ", genre=" + genre +
                '}';
    }
}
