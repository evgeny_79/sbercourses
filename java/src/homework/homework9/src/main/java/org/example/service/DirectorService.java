package org.example.service;

import org.example.dto.AddFilmDTO;
import org.example.models.Director;
import org.example.repository.DirectorRepository;
import org.example.repository.FilmRepository;
import org.springframework.stereotype.Service;

@Service
public class DirectorService extends GenericService<Director>{

    private final DirectorRepository directorRepository;
    private final FilmService filmService;

    public DirectorService(DirectorRepository directorRepository, FilmRepository filmRepository, FilmService filmService) {
        super(directorRepository);
        this.directorRepository = directorRepository;
        this.filmService = filmService;
    }

    public void addFilm(AddFilmDTO addFilmDTO) {
        Director director = getOne(addFilmDTO.getDirectorID());
        director.getFilms().add(filmService.getOne(addFilmDTO.getFilmID()));
        update(director);
    }
}
