package org.example.mapper;

import jakarta.annotation.PostConstruct;
import org.example.dto.OrderDTO;
import org.example.models.Order;
import org.example.repository.FilmRepository;
import org.example.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO>{

    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    public OrderMapper(ModelMapper modelMapper, FilmRepository filmRepository, UserRepository userRepository) {
        super(modelMapper, Order.class, OrderDTO.class);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserID)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OrderDTO::setFilmID)).setPostConverter(toDtoConverter());

        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmID()).orElseThrow(() -> new NoSuchElementException("Книга не найдена")));
        destination.setUser(userRepository.findById(source.getUserID()).orElseThrow(() -> new NoSuchElementException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserID(source.getUser().getId());
        destination.setFilmID(source.getFilm().getId());
    }
}
