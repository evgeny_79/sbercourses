package org.example.mapper;

import jakarta.annotation.PostConstruct;
import org.example.dto.DirectorDTO;
import org.example.models.Director;
import org.example.models.GenericModel;
import org.example.repository.FilmRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Director, DirectorDTO> {

    private final FilmRepository filmRepository;

    protected DirectorMapper(ModelMapper modelMapper, FilmRepository filmRepository) {
        super(modelMapper, Director.class, DirectorDTO.class);
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorDTO.class)
                .addMappings(m -> m.skip(DirectorDTO::setFilmsID)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(DirectorDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DirectorDTO source, Director destination) {
        if(!Objects.isNull(source.getFilmsID())) {
            destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmsID())));
        }
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorDTO destination) {
        destination.setFilmsID(getFilmsIds(source));
    }


    protected Set<Long> getFilmsIds(Director entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getFilms())
                ? null
                : entity.getFilms()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
