package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.dto.UserDTO;
import org.example.mapper.GenericMapper;
import org.example.mapper.UserMapper;
import org.example.models.Director;
import org.example.models.User;
import org.example.repository.UserRepository;
import org.example.service.GenericService;
import org.example.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями фильмотеки")
public class UserController extends GenericController<User, UserDTO>{

    public UserController(UserService userService, UserMapper userMapper) {
        super(userService, userMapper);
    }




}
