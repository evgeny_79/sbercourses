package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.dto.DirectorDTO;
import org.example.mapper.DirectorMapper;
import org.example.models.Director;
import org.example.models.Film;
import org.example.repository.FilmRepository;
import org.example.service.DirectorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Tag(name = "Режиссеры", description = "Контроллер для работы с режиссерами фильмов фильмотеки")
@RequestMapping("/directors")
public class DirectorController extends GenericController<Director, DirectorDTO> {

    private final FilmRepository filmRepository;
    private final DirectorService directorService;
    private final DirectorMapper directorMapper;


    protected DirectorController(DirectorService directorService, DirectorMapper directorMapper, FilmRepository filmRepository) {
        super(directorService, directorMapper);
        this.filmRepository = filmRepository;
        this.directorService = directorService;
        this.directorMapper = directorMapper;
    }

    @Operation(description = "Добавить фильм режиссеру", method = "addFilm")
    @PostMapping(value = "/addFilm", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestBody Long filmID,
                                               @RequestParam Long directorID) {
        try {
            Film film = filmRepository.findById(filmID).orElseThrow(() -> new NoSuchElementException("Фильм с переданным ID не найден"));
            Director director = directorService.getOne(directorID);
            director.getFilms().add(film);
            return ResponseEntity.ok(directorMapper.toDTO(directorService.update(director)));
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }
}
