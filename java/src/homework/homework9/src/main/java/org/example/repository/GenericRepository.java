package org.example.repository;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
@Hidden
public interface GenericRepository<T> extends JpaRepository<T, Long> {
}
