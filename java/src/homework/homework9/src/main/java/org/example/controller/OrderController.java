package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.dto.OrderDTO;
import org.example.mapper.OrderMapper;
import org.example.models.Order;
import org.example.service.OrderService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "Аренда/покупка фильмов",
        description = "Контроллер для работы с арендой/покупкой фильмов пользователям фильмотеки")
@RequestMapping("/orders")
public class OrderController extends GenericController<Order, OrderDTO>{

    private  final OrderService orderService;
    private final OrderMapper orderMapper;

    public OrderController(OrderService orderService, OrderMapper orderMapper) {
        super(orderService, orderMapper);
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    @Operation(description = "Просмотреть список фильмов пользователя", method = "getUserRentFilmInfo")
    @GetMapping(value = "/get-user-rent-film-info/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrderDTO>> getUserRentFilmInfo(@PathVariable Long userId) {
        return ResponseEntity.ok(orderMapper.toDTOs(orderService.getUserFilmRentInfo(userId)));
    }
}
