package org.example.repository;

import io.swagger.v3.oas.annotations.Hidden;
import org.example.models.Order;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface OrderRepository extends GenericRepository<Order>{
}
