package org.example.dto;

import org.example.models.Genre;
import java.time.LocalDate;
import java.util.Set;

public class FilmDTO extends GenericDTO{
    private String title;
    private LocalDate premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsID ;

    public FilmDTO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getPremierYear() {
        return premierYear;
    }

    public void setPremierYear(LocalDate premierYear) {
        this.premierYear = premierYear;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Set<Long> getDirectorsID() {
        return directorsID;
    }

    public void setDirectorsID(Set<Long> directorsID) {
        this.directorsID = directorsID;
    }
}
