package org.example.models;

import jakarta.persistence.*;

@Entity
@Table(name = "Role")
public class Role extends GenericModel{

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    public Role() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
