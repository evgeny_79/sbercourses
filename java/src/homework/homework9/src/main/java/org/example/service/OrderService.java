package org.example.service;

import org.example.models.Order;
import org.example.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService extends GenericService<Order>{

    private final UserService userService;
    private final OrderRepository orderRepository;

    public OrderService(UserService userService, OrderRepository orderRepository) {
        super(orderRepository);
        this.userService = userService;
        this.orderRepository = orderRepository;
    }

    public List<Order> getUserFilmRentInfo(Long id){
        return userService.getOne(id).getOrders().stream().toList();
    }
}
