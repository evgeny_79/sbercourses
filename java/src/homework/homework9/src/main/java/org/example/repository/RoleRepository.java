package org.example.repository;

import io.swagger.v3.oas.annotations.Hidden;
import org.example.models.Role;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface RoleRepository extends GenericRepository<Role>{
}
