package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.dto.FilmDTO;
import org.example.mapper.FilmMapper;
import org.example.models.Director;
import org.example.models.Film;
import org.example.repository.DirectorRepository;
import org.example.service.FilmService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/films")
public class FilmController extends GenericController<Film, FilmDTO>{

    private final DirectorRepository directorRepository;
    private final FilmService filmService;
    private final FilmMapper filmMapper;

    protected FilmController(FilmService filmService, FilmMapper filmMapper, DirectorRepository directorRepository) {
        super(filmService, filmMapper);
        this.directorRepository = directorRepository;
        this.filmService = filmService;
        this.filmMapper = filmMapper;
    }

    @Operation(description = "Добавить фильму режиссера", method = "addDirector")
    @PostMapping(value = "/addDirector", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addDirector(@RequestBody Long filmID,
                                               @RequestParam Long directorID) {
        try {
            Director director = directorRepository.findById(directorID).orElseThrow(() -> new NoSuchElementException("Директор с переданным ID не найден"));
            Film film = filmService.getOne(filmID);
            film.getDirectors().add(director);
            return ResponseEntity.ok(filmMapper.toDTO(filmService.update(film)));
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }
}
