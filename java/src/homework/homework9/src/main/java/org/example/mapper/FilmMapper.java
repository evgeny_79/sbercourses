package org.example.mapper;

import jakarta.annotation.PostConstruct;
import org.example.dto.DirectorDTO;
import org.example.dto.FilmDTO;
import org.example.models.Director;
import org.example.models.Film;
import org.example.models.GenericModel;
import org.example.repository.DirectorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO>{

    private final DirectorRepository directorRepository;

    protected FilmMapper(ModelMapper modelMapper, DirectorRepository directorRepository) {
        super(modelMapper, Film.class, FilmDTO.class);
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorsID)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if(!Objects.isNull(source.getDirectorsID())) {
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorsID())));
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorsID(getDirectorsIDs(source));
    }


    protected Set<Long> getDirectorsIDs(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getDirectors())
                ? null
                : entity.getDirectors()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
