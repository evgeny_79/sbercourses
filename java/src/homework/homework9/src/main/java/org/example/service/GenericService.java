package org.example.service;

import org.example.models.GenericModel;
import org.example.repository.GenericRepository;

import java.util.List;
import java.util.NoSuchElementException;

public abstract class GenericService <T extends GenericModel>{

    private final GenericRepository<T> genericRepository;

    protected GenericService(GenericRepository<T> genericRepository) {
        this.genericRepository = genericRepository;
    }

    public List<T> listAll() {
        return genericRepository.findAll();
    }

    public T getOne(Long id) {
        return genericRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Запись с таким id не найдена"));
    }

    public T create(T object) {
        return genericRepository.save(object);
    }

    public T update(T object) {
        return genericRepository.save(object);
    }

    public void delete(Long id) {
        genericRepository.deleteById(id);
    }

    public boolean existsById(Long id){
        return genericRepository.existsById(id);
    }
}
