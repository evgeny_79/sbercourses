package org.example.dto;

public abstract class GenericDTO {

    protected Long id;

    public GenericDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
