package org.example.repository;

import io.swagger.v3.oas.annotations.Hidden;
import org.example.models.Director;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface DirectorRepository extends GenericRepository<Director>{
}
