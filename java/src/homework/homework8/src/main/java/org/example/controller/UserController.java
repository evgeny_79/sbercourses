package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.models.Director;
import org.example.models.User;
import org.example.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Operation(description = "Добавить пользователя", method = "create")
    @PostMapping(value = "/add", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> create(@RequestBody User newEntity) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(userRepository.save(newEntity));
    }

    @Operation(description = "Получить пользователя по ID", method = "getById")
    @GetMapping(value = "/getById", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getById(@RequestParam Long id) {
        return userRepository.findById(id)
                .map(t -> ResponseEntity.status(OK).body(t))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Operation(description = "Обновить пользователя", method = "update")
    @PutMapping(value = "/update", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> update(@RequestBody User updatedEntity,
                                           @RequestParam Long id) {
        updatedEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(userRepository.save(updatedEntity));
    }

    @Operation(description = "Удалить пользователя по ID", method = "delete")
    @DeleteMapping( "/delete/{id}")
    public void delete(@PathVariable Long id) {
        userRepository.deleteById(id);
    }
}
