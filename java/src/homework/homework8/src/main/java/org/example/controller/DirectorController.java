package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.models.Director;
import org.example.repository.DirectorRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/directors")
public class DirectorController {

    private final DirectorRepository directorRepository;

    public DirectorController(DirectorRepository directorRepository) {
        this.directorRepository = directorRepository;
    }

    @Operation(description = "Добавить режиссера", method = "create")
    @PostMapping(value = "/add", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> create(@RequestBody Director newEntity) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(directorRepository.save(newEntity));
    }

    @Operation(description = "Получить режиссера по ID", method = "getById")
    @GetMapping(value = "/getById", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> getById(@RequestParam Long id) {
        return directorRepository.findById(id)
                .map(t -> ResponseEntity.status(OK).body(t))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Operation(description = "Обновить режиссера", method = "update")
    @PutMapping(value = "/update", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> update(@RequestBody Director updatedEntity,
                                       @RequestParam Long id) {
        updatedEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(directorRepository.save(updatedEntity));
    }

    @Operation(description = "Удалить режиссера по ID", method = "delete")
    @DeleteMapping( "/delete/{id}")
    public void delete(@PathVariable Long id) {
        directorRepository.deleteById(id);
    }
}
