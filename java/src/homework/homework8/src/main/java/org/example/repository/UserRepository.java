package org.example.repository;

import io.swagger.v3.oas.annotations.Hidden;
import org.example.models.User;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface UserRepository extends GenericRepository<User>{
}
