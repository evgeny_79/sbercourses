package org.example.models;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "directors")
public class Director {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "directors_fio")
    private String directorFIO;

    @Column(name = "position")
    private int position;

    /* по умолчанию Lazy */
    @ManyToMany(mappedBy = "directors", cascade = CascadeType.ALL)
    Set<Film> films = new HashSet<>();

    public Director() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDirectorFIO() {
        return directorFIO;
    }

    public void setDirectorFIO(String directorFIO) {
        this.directorFIO = directorFIO;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Set<Film> getFilms() {
        return films;
    }

    public void setFilms(Set<Film> films) {
        this.films = films;
    }

    @Override
    public String toString() {
        return "Director{" +
                "id=" + id +
                ", directorFIO='" + directorFIO + '\'' +
                ", position=" + position +
                '}';
    }
}

