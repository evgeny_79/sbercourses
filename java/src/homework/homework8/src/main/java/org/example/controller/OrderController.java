package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.models.Order;
import org.example.models.User;
import org.example.repository.OrderRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderRepository orderRepository;

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Operation(description = "Добавить order", method = "create")
    @PostMapping(value = "/add", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> create(@RequestBody Order newEntity) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(orderRepository.save(newEntity));
    }

    @Operation(description = "Получить order по ID", method = "getById")
    @GetMapping(value = "/getById", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> getById(@RequestParam Long id) {
        return orderRepository.findById(id)
                .map(t -> ResponseEntity.status(OK).body(t))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Operation(description = "Обновить order", method = "update")
    @PutMapping(value = "/update", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> update(@RequestBody Order updatedEntity,
                                       @RequestParam Long id) {
        updatedEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(orderRepository.save(updatedEntity));
    }

    @Operation(description = "Удалить order по ID", method = "delete")
    @DeleteMapping( "/delete/{id}")
    public void delete(@PathVariable Long id) {
        orderRepository.deleteById(id);
    }
}
