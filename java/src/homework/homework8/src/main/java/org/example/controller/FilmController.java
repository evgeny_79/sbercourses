package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.models.Film;
import org.example.repository.FilmRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/films")
public class FilmController {

    private final FilmRepository filmRepository;

    public FilmController(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Operation(description = "Добавить фильм", method = "create")
    @PostMapping(value = "/add", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> create(@RequestBody Film newEntity) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(filmRepository.save(newEntity));
    }

    @Operation(description = "Получить фильм по ID", method = "getById")
    @GetMapping(value = "/getById", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> getById(@RequestParam Long id) {
        return filmRepository.findById(id)
                .map(t -> ResponseEntity.status(OK).body(t))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Operation(description = "Обновить фильм", method = "update")
    @PutMapping(value = "/update", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> update(@RequestBody Film updatedEntity,
                                    @RequestParam Long id) {
        updatedEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(filmRepository.save(updatedEntity));
    }

    @Operation(description = "Удалить фильм по ID", method = "delete")
    @DeleteMapping( "/delete/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        filmRepository.deleteById(id);
    }
}
