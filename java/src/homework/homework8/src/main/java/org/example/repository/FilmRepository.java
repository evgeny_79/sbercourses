package org.example.repository;

import io.swagger.v3.oas.annotations.Hidden;
import org.example.models.Film;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface FilmRepository extends GenericRepository<Film>{
}
