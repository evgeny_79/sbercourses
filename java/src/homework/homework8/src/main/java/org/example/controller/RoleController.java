package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.models.Role;
import org.example.models.User;
import org.example.repository.RoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/role")
public class RoleController {

    private final RoleRepository roleRepository;

    public RoleController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Operation(description = "Добавить role", method = "create")
    @PostMapping(value = "/add", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Role> create(@RequestBody Role newEntity) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(roleRepository.save(newEntity));
    }

    @Operation(description = "Получить role по ID", method = "getById")
    @GetMapping(value = "/getById", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Role> getById(@RequestParam Long id) {
        return roleRepository.findById(id)
                .map(t -> ResponseEntity.status(OK).body(t))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Operation(description = "Обновить role", method = "update")
    @PutMapping(value = "/update", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Role> update(@RequestBody Role updatedEntity,
                                       @RequestParam Long id) {
        updatedEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(roleRepository.save(updatedEntity));
    }

    @Operation(description = "Удалить role по ID", method = "delete")
    @DeleteMapping( "/delete/{id}")
    public void delete(@PathVariable Long id) {
        roleRepository.deleteById(id);
    }
}
