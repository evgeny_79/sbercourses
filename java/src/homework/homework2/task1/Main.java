package src.homework.homework2.task1;

import java.util.ArrayList;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Moscow");
        list.add("Moscow");
        list.add("Moscow");
        list.add("Tver");
        System.out.println(list);

        System.out.println(uniqueValue(list));
    }

    private static <E> HashSet<E> uniqueValue(ArrayList<E> list) {
        return new HashSet<>(list);
    }
}
