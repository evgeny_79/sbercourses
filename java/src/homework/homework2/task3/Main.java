package src.homework.homework2.task3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        PowerfulSet powerfulSet = new PowerfulSet();

        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set1.add(5);
        System.out.println("set1: " + set1);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);
        System.out.println("set2: " + set2);

        System.out.println("пересечение: " + powerfulSet.intersection(set1, set2));
        System.out.println("объединение: " + powerfulSet.union(set1, set2));
        System.out.println("разность: " + powerfulSet.relativeComplement(set1, set2));
    }
}
