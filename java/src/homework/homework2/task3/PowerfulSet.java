package src.homework.homework2.task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> s1 = new HashSet<>(set1);
        Set<T> s2 = new HashSet<>(set2);
        s1.retainAll(s2);
        return s1;
    }
    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> s1 = new HashSet<>(set1);
        Set<T> s2 = new HashSet<>(set2);
        s1.addAll(s2);
        return s1;
    }
    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> s1 = new HashSet<>(set1);
        Set<T> s2 = new HashSet<>(set2);
        s1.removeAll(s2);
        return s1;
    }
}
