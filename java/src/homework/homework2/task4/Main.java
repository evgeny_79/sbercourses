package src.homework.homework2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Document> documents = new ArrayList<>();
        documents.add(new Document(1, "Ivan", 5));
        documents.add(new Document(2, "Sergei", 10));

        Map<Integer, Document> organizedDocuments = organizeDocuments(documents);

        System.out.println(organizedDocuments.get(1));
    }

    private static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document document: documents){
            map.put(document.getId(), document);
        }
        return map;
    }
}
