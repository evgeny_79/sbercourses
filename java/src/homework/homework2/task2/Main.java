package src.homework.homework2.task2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        System.out.println(isAnagram(str1, str2));
    }

    private static boolean isAnagram(String string1, String string2) {
        return preprocess(string1).equals(preprocess(string2));
    }

    private static String preprocess(String source) {
        char[] chars = source.replaceAll("\\s", "").toLowerCase().toCharArray();
        Arrays.sort(chars);
        return Arrays.toString(chars);
    }

}
