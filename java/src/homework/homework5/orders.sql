CREATE TABLE orders
(
    id           SERIAL PRIMARY KEY,
    flower_id    INT REFERENCES flower (id),
    count        INT       NOT NULL CHECK ( count >= 1 AND count <= 1000),
    customer_id  INT REFERENCES customer (id),
    created_date TIMESTAMP NOT NULL
);

INSERT INTO orders (flower_id, count, customer_id, created_date)
VALUES (1, 11, 3, '2023-01-14T18:07'),
       (2, 21, 1, '2023-02-14T10:07'),
       (3, 21, 2, '2023-01-14T10:07'),
       (1, 15, 1, '2023-01-14T15:07'),
       (3, 5, 3, '2023-02-14T12:07'),
       (1, 31, 3, '2023-02-20T12:07');