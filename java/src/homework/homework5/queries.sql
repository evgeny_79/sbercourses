-- 1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
SELECT *
FROM orders o
         INNER JOIN customer c on c.id = o.customer_id
WHERE o.id = 1;

-- 2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
SELECT *
FROM orders o
         INNER JOIN customer c on c.id = o.customer_id
WHERE  extract(month from o.created_date) = extract(month from now()) AND c.id = 3;

-- 3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
SELECT f.name, o.count
FROM orders o
         INNER JOIN flower f on f.id = o.flower_id
WHERE count = (SELECT max(count) FROM orders);

-- 4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
SELECT sum(o.count * f.cost) AS total_income
FROM orders o
         INNER JOIN flower f on f.id = o.flower_id;