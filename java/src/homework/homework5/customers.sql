CREATE TABLE customer
(
    id    SERIAL PRIMARY KEY,
    name  VARCHAR(128) NOT NULL,
    phone VARCHAR(16)  NOT NULL
);

INSERT INTO customer (name, phone)
VALUES ('Evgeny', '1111111111'),
       ('Sveta', '2222222222'),
       ('Sergey', '3333333333');