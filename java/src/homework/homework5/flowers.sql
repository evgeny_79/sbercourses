CREATE TABLE flower
(
    id    SERIAL PRIMARY KEY,
    name  VARCHAR(128) NOT NULL,
    cost  NUMERIC NOT NULL
);

INSERT INTO flower (name, cost)
VALUES ('Роза', 100),
       ('Лилия', 50),
       ('Ромашка', 25);