package sber.school.video.REST.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import sber.school.video.dto.RoleDto;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@Transactional
public class RESTRoleControllerTest extends GenericControllerTest {
    private static final String BASE_URL = "/rest/roles";

    @Test
    @Override
    public void getAll() throws Exception {
        String result = super.mvc.perform(
                get(BASE_URL)
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        List<RoleDto> directorDtoList = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        directorDtoList.forEach(roleDto -> log.info(roleDto.getTitle().toString()));
    }

    @Test
    @Override
    public void getOne() throws Exception {
        RoleDto roleDto = modelService.createRole();

        String result = super.mvc.perform(
                get(BASE_URL + "/{id}", roleDto.getId())
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        RoleDto receivedDto = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        assertEquals(receivedDto.getId(), roleDto.getId());
    }

    @Test
    @Override
    public void create() throws Exception {
        RoleDto roleDto = modelService.newRole();

        String result = mvc.perform(
                post(BASE_URL)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(roleDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        RoleDto created = objectMapper.readValue(result, RoleDto.class);

        assertEquals(roleDto.getTitle(), created.getTitle());
    }


    @Test
    @Override
    void deleteOne() throws Exception {
        RoleDto roleDto = modelService.createRole();

        RoleDto checkDto = modelService.getRoleService().findOne(roleDto.getId());
        assertNotNull(checkDto);

        mvc.perform(
                delete(BASE_URL + "/{id}", roleDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        try {
            checkDto = modelService.getRoleService().findOne(roleDto.getId());
        } catch (NotFoundException e) {
            checkDto = null;
        }

        assertNull(checkDto);
    }

    @Test
    @Override
    void update() throws Exception {
        RoleDto roleDto = modelService.createRole();
        roleDto.setTitle("UPDATED");

        String result = mvc.perform(
                put(BASE_URL + "/{id}", roleDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(roleDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        RoleDto updatedDto = objectMapper.readValue(result, RoleDto.class);

        assertEquals(updatedDto.getId(), roleDto.getId());
        assertEquals(updatedDto.getTitle(), roleDto.getTitle());
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
