package sber.school.video.REST.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import sber.school.video.dto.DirectorDto;
import sber.school.video.dto.FilmDto;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@Transactional
public class RESTFilmControllerTest extends GenericControllerTest {
    private static final String BASE_URL = "/rest/films";

    @Test
    @Override
    public void getAll() throws Exception {
        String result = super.mvc.perform(
                get(BASE_URL)
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        List<FilmDto> DTOList = objectMapper.readValue(
            result, new TypeReference<>() {
            }
        );

        DTOList.forEach(filmDto -> log.info(filmDto.getTitle()));
    }

    @Test
    @Override
    public void getOne() throws Exception {
        FilmDto filmDto = modelService.createFilm();

        String result = super.mvc.perform(
                get(BASE_URL + "/{id}", filmDto.getId())
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        FilmDto receivedDto = objectMapper.readValue(
            result, new TypeReference<>() {
            }
        );

        assertEquals(receivedDto.getId(), filmDto.getId());
    }

    @Test
    @Override
    public void create() throws Exception {
        FilmDto filmDto = modelService.newFilm();

        String result = mvc.perform(
                post(BASE_URL)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(filmDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        FilmDto created = objectMapper.readValue(result, FilmDto.class);

        assertEquals(filmDto.getTitle(), created.getTitle());
        assertEquals(filmDto.getCountry(), created.getCountry());
        assertEquals(filmDto.getPremierYear(), created.getPremierYear());
    }


    @Test
    @Override
    void deleteOne() throws Exception {
        FilmDto filmDto = modelService.createFilm();

        FilmDto checkDto = modelService.getFilmService().findOne(filmDto.getId());
        assertNotNull(checkDto);

        mvc.perform(
                delete(BASE_URL + "/{id}", filmDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        try {
            checkDto = modelService.getFilmService().findOne(filmDto.getId());
        } catch (NotFoundException e) {
            checkDto = null;
        }

        assertNull(checkDto);
    }

    @Test
    @Override
    void update() throws Exception {
        FilmDto filmDto = modelService.createFilm();
        filmDto.setCountry("UpdatedData");

        String result = mvc.perform(
                put(BASE_URL + "/{id}", filmDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(filmDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        FilmDto updatedDto = objectMapper.readValue(result, FilmDto.class);

        assertEquals(updatedDto.getId(), filmDto.getId());
        assertEquals(updatedDto.getCountry(), filmDto.getCountry());
        assertEquals(updatedDto.getPremierYear(), filmDto.getPremierYear());
    }

    @Test
    void addDirector() throws Exception {
        FilmDto filmDto = modelService.createFilm();
        DirectorDto directorDto = modelService.createDirector();

        int initialDirectorCount = filmDto.getDirectorsId().size();

        String result = mvc.perform(
                put(BASE_URL + "/add-director/film/{filmId}/director/{directorId}",
                    filmDto.getId(),
                    directorDto.getId()
                )
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        FilmDto updatedDto = objectMapper.readValue(result, FilmDto.class);

        assertEquals(updatedDto.getDirectorsId().size(), initialDirectorCount + 1);
    }

    @Test
    void addDirectorError() throws Exception {
        mvc.perform(
                put(BASE_URL + "/add-director/film/{filmId}/director/{directorId}", -1, -1)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString();
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
