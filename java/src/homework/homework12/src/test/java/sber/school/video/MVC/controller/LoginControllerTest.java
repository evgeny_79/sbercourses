package sber.school.video.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
public class LoginControllerTest extends AbstractTest {
    @Test
    @DisplayName("login:form")
    protected void login() throws Exception {
        mvc.perform(
                get("/login")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name( "main/login"))
            .andReturn();
    }


    @Test
    @DisplayName("login:redirect")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void loginRedirect() throws Exception {
        mvc.perform(
                get("/login")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrlTemplate("/"))
            .andReturn();
    }
}
