package sber.school.video.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
public class MainControllerTest extends AbstractTest {
    @Test
    @DisplayName("main:page")
    protected void login() throws Exception {
        mvc.perform(
                get("/")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name( "main/main"))
            .andReturn();
    }
}
