package sber.school.video.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import sber.school.video.dto.FilmDto;
import sber.school.video.service.ModelService;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
public class FilmControllerTest extends CommonTest {
    @Autowired
    private ModelService modelService;

    @Test
    @Override
    @DisplayName("film:list")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void getAll() throws Exception {
        mvc.perform(
                get("/film/list")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("film/list"))
            .andExpect(model().attributeExists("films"))
            .andReturn();
    }

    @Test
    @Override
    @DisplayName("film:form")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void form() throws Exception {
        mvc.perform(
                get("/film/form")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("film/form"))
            .andExpect(model().attributeExists("film"))
            .andReturn();
    }

    @Test
    @Override
    @DisplayName("film:create")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void save() throws Exception {
        mvc.perform(
            post("/film/save")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .flashAttr("filmForm", modelService.newFilm())
                .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/film/list"))
            .andExpect(redirectedUrlTemplate("/film/list"));
    }

    @Test
    @Override
    @DisplayName("film:update")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void update() throws Exception {
        FilmDto filmDto = modelService.createFilm();

        mvc.perform(
                post("/film/save")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .flashAttr("filmForm", filmDto)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/film/list"))
            .andExpect(redirectedUrlTemplate("/film/list"));
    }
}
