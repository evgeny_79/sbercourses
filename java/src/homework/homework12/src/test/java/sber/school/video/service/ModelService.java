package sber.school.video.service;

import lombok.Getter;
import org.springframework.stereotype.Service;
import sber.school.video.dto.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.UUID;

@Getter
@Service
public class ModelService {
    private final DirectorService directorService;
    private final FilmService filmService;
    private final GenreService genreService;
    private final OrderService orderService;
    private final RoleService roleService;
    private final UserService userService;

    public ModelService(
        DirectorService directorService,
        FilmService filmService,
        GenreService genreService,
        OrderService orderService,
        RoleService roleService,
        UserService userService
    ) {
        this.directorService = directorService;
        this.filmService = filmService;
        this.genreService = genreService;
        this.orderService = orderService;
        this.roleService = roleService;
        this.userService = userService;
    }

    public DirectorDto newDirector() {
        return new DirectorDto(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            new HashSet<>()
        );
    }

    public DirectorDto createDirector() {
        return directorService.create(newDirector());
    }

    public FilmDto newFilm() {
        return new FilmDto(
            UUID.randomUUID().toString(), 2023, UUID.randomUUID().toString(),
            new HashSet<>(), new HashSet<>(), new HashSet<>()
        );
    }

    public GenreDto newGenre() {
        return new GenreDto(
            UUID.randomUUID().toString()
        );
    }

    public GenreDto createGenre() {
        return genreService.create(newGenre());
    }

    public FilmDto createFilm() {
        GenreDto genreDto = createGenre();
        DirectorDto directorDto = createDirector();

        FilmDto filmDto = newFilm();
        filmDto.getGenresId().add(genreDto.getId());
        filmDto.getDirectorsId().add(directorDto.getId());

        return filmService.create(filmDto);
    }

    public UserDto createUser() {
        return userService.create(newUser());
    }

    public UserDto newUser() {
        return new UserDto(
            UUID.randomUUID().toString(),
            "123",
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            LocalDate.now(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            1L,
            new HashSet<>()
        );
    }

    public OrderDto newOrder() {
        return new OrderDto(
            LocalDate.now(), 3, false, null, null
        );
    }

    public OrderDto createOrder() {
        UserDto userDto = createUser();
        FilmDto filmDto = createFilm();

        OrderDto orderDto = newOrder();
        orderDto.setUserId(userDto.getId());
        orderDto.setFilmId(filmDto.getId());

        return orderService.create(orderDto);
    }

    public RoleDto newRole() {
        return new RoleDto(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        );
    }

    public RoleDto createRole() {
        return roleService.create(newRole());
    }
}
