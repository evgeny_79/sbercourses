package sber.school.video.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import sber.school.video.dto.FilmDto;
import sber.school.video.dto.OrderDto;
import sber.school.video.dto.UserDto;
import sber.school.video.service.ModelService;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
public class OrderControllerTest extends AbstractTest {
    @Autowired
    private ModelService modelService;

    @Test
    @DisplayName("order:list")
    @WithMockUser(username = "admin", password = "admin", authorities = {"ROLE_STAFF"})
    protected void getAll() throws Exception {
        mvc.perform(
                get("/order/list")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("order/list"))
            .andExpect(model().attributeExists("orders"))
            .andReturn();
    }

    @Test
    @DisplayName("order:form")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void form() throws Exception {
        FilmDto filmDto = modelService.createFilm();

        mvc.perform(
                get("/order/create/" + filmDto.getId())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(model().attributeExists("film"))
            .andExpect(model().attributeExists("order"))
            .andExpect(view().name("order/create"))
            .andReturn();
    }

    @Test
    @DisplayName("order:form:redirect")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void formRedirect() throws Exception {
        mvc.perform(
                get("/order/create/" + 0)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/film/list"))
            .andExpect(redirectedUrlTemplate("/film/list"))
            .andReturn();
    }

    @Nested
    class TestWithUser {
        @BeforeEach
        public void init() {
            UserDto userDto = modelService.newUser();
            userDto.setLogin("_uniqueTestUser123");

            modelService.getUserService().create(userDto);
        }

        @Test
        @DisplayName("order:list:user")
        @WithMockUser(username = "_uniqueTestUser123", password = "admin", authorities = {"ROLE_USER"})
        protected void getUserOrders() throws Exception {
            mvc.perform(
                    get("/order/list")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("order/list"))
                .andExpect(model().attributeExists("orders"))
                .andReturn();
        }

        @Test
        @DisplayName("order:create")
        @WithMockUser(username = "_uniqueTestUser123", password = "admin", roles = "USER")
        protected void save() throws Exception {
            FilmDto filmDto = modelService.createFilm();

            OrderDto orderDto = modelService.newOrder();
            orderDto.setFilmId(filmDto.getId());
            orderDto.setIsPurchased(null);

            mvc.perform(
                    post("/order/create")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("orderForm", orderDto)
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/order/list"))
                .andExpect(redirectedUrlTemplate("/order/list"));
        }

        @Test
        @DisplayName("order:create:purchase")
        @WithMockUser(username = "_uniqueTestUser123", password = "admin", roles = "USER")
        protected void savePurchase() throws Exception {
            FilmDto filmDto = modelService.createFilm();

            OrderDto orderDto = modelService.newOrder();
            orderDto.setFilmId(filmDto.getId());
            orderDto.setIsPurchased(true);

            mvc.perform(
                    post("/order/create")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("orderForm", orderDto)
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/order/list"))
                .andExpect(redirectedUrlTemplate("/order/list"));
        }
    }
}
