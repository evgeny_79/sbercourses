package sber.school.video.REST.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import sber.school.video.dto.FilmDto;
import sber.school.video.dto.OrderDto;
import sber.school.video.dto.UserDto;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@Transactional
public class RESTOrderControllerTest extends GenericControllerTest {
    private static final String BASE_URL = "/rest/orders";

    @Test
    @Override
    public void getAll() throws Exception {
        String result = super.mvc.perform(
                get(BASE_URL)
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        List<OrderDto> directorDtoList = objectMapper.readValue(
            result, new TypeReference<>(){}
        );

        directorDtoList.forEach(orderDto -> log.info(orderDto.getUserId().toString()));
    }

    @Test
    @Override
    public void getOne() throws Exception {
        OrderDto orderDto = modelService.createOrder();

        String result = super.mvc.perform(
                get(BASE_URL + "/{id}", orderDto.getId())
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        OrderDto receivedDto = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        assertEquals(receivedDto.getId(), orderDto.getId());
    }

    @Test
    @Override
    public void create() throws Exception {
        FilmDto filmDto = modelService.createFilm();
        UserDto userDto = modelService.createUser();

        OrderDto orderDto = modelService.newOrder();
        orderDto.setFilmId(filmDto.getId());
        orderDto.setUserId(userDto.getId());

        String result = mvc.perform(
                post(BASE_URL)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(orderDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        OrderDto created = objectMapper.readValue(result, OrderDto.class);

        assertEquals(orderDto.getFilmId(), created.getFilmId());
        assertEquals(orderDto.getUserId(), created.getUserId());
    }


    @Test
    @Override
    void deleteOne() throws Exception {
        OrderDto orderDto = modelService.createOrder();

        OrderDto checkDto = modelService.getOrderService().findOne(orderDto.getId());
        assertNotNull(checkDto);

        mvc.perform(
                delete(BASE_URL + "/{id}", orderDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        try {
            checkDto = modelService.getOrderService().findOne(orderDto.getId());
        } catch (NotFoundException e) {
            checkDto = null;
        }

        assertNull(checkDto);
    }

    @Test
    @Override
    void update() throws Exception {
        OrderDto orderDto = modelService.createOrder();
        orderDto.setRentPeriod(99);

        String result = mvc.perform(
                put(BASE_URL + "/{id}", orderDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(orderDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        OrderDto updatedDto = objectMapper.readValue(result, OrderDto.class);

        assertEquals(updatedDto.getId(), orderDto.getId());
        assertEquals(updatedDto.getRentPeriod(), orderDto.getRentPeriod());
        assertEquals(updatedDto.getUserId(), orderDto.getUserId());
        assertEquals(updatedDto.getFilmId(), orderDto.getFilmId());
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
