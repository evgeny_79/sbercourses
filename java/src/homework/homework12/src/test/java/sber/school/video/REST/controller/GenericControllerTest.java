package sber.school.video.REST.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sber.school.video.config.jwt.JWTTokenUtil;
import sber.school.video.service.ModelService;
import sber.school.video.service.userdetails.CustomUserDetailsService;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestInstance(Lifecycle.PER_CLASS)
@Transactional
public abstract class GenericControllerTest {
    @Autowired
    protected MockMvc mvc;
    @Autowired
    protected JWTTokenUtil jwtTokenUtil;
    @Autowired
    protected CustomUserDetailsService userDetailsService;
    protected String token = "";
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected ModelService modelService;
    protected HttpHeaders headers = new HttpHeaders();

    protected String generateToken(String username) {
        return jwtTokenUtil.generateToken(userDetailsService.loadUserByUsername(username));
    }

    @BeforeAll
    public void prepare() {
        token = generateToken("admin");
        headers.add("Authorization", "Bearer " + token);
    }

    abstract void getAll() throws Exception;

    abstract void getOne() throws Exception;

    abstract  void create() throws Exception;

    abstract void deleteOne() throws Exception;

    abstract void update() throws Exception;
}
