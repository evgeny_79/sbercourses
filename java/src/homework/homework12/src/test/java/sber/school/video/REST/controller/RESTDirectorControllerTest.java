package sber.school.video.REST.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import java.nio.charset.StandardCharsets;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import sber.school.video.dto.DirectorDto;
import sber.school.video.dto.FilmDto;

@Slf4j
@Transactional
public class RESTDirectorControllerTest extends GenericControllerTest {
    private static final String BASE_URL = "/rest/directors";

    @Test
    @Override
    public void getAll() throws Exception {
        String result = super.mvc.perform(
                get(BASE_URL)
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        List<DirectorDto> directorDtoList = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        directorDtoList.forEach(directorDto -> log.info(directorDto.getFullName()));
    }

    @Test
    @Override
    public void getOne() throws Exception {
        DirectorDto directorDto = modelService.createDirector();

        String result = super.mvc.perform(
                get(BASE_URL + "/{id}", directorDto.getId())
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        DirectorDto receivedDto = objectMapper.readValue(
            result, new TypeReference<>() {
            }
        );

        assertEquals(receivedDto.getId(), directorDto.getId());
    }

    @Test
    @Override
    public void create() throws Exception {
        DirectorDto directorDto = modelService.newDirector();

        String result = mvc.perform(
                post(BASE_URL)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(directorDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        DirectorDto created = objectMapper.readValue(result, DirectorDto.class);

        assertEquals(directorDto.getFullName(), created.getFullName());
        assertEquals(directorDto.getPosition(), created.getPosition());
    }

    @Test
    public void createError() throws Exception {
        DirectorDto directorDto = modelService.createDirector();

        mvc.perform(
                post(BASE_URL)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(directorDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isConflict())
            .andReturn()
            .getResponse()
            .getContentAsString();
    }


    @Test
    @Override
    void deleteOne() throws Exception {
        DirectorDto directorDto = modelService.createDirector();

        DirectorDto checkDto = modelService.getDirectorService().findOne(directorDto.getId());
        assertNotNull(checkDto);

        mvc.perform(
                delete(BASE_URL + "/{id}", directorDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        try {
            checkDto = modelService.getDirectorService().findOne(directorDto.getId());
        } catch (NotFoundException e) {
            checkDto = null;
        }

        assertNull(checkDto);
    }

    @Test
    @Override
    void update() throws Exception {
        DirectorDto directorDto = modelService.createDirector();
        directorDto.setPosition("UpdatedData");

        String result = mvc.perform(
                put(BASE_URL + "/{id}", directorDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(directorDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        DirectorDto updatedDto = objectMapper.readValue(result, DirectorDto.class);

        assertEquals(updatedDto.getId(), directorDto.getId());
        assertEquals(updatedDto.getFullName(), directorDto.getFullName());
        assertEquals(updatedDto.getPosition(), directorDto.getPosition());
    }

    @Test
    void updateError() throws Exception {
        DirectorDto directorDto = modelService.createDirector();
        directorDto.setPosition("UpdatedData");

        mvc.perform(
                put(BASE_URL + "/{id}", -1)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(directorDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString();
    }

    @Test
    void addFilm() throws Exception {
        DirectorDto directorDto = modelService.createDirector();
        FilmDto filmDto = modelService.createFilm();

        int initialFimCount = directorDto.getFilmsId().size();

        String result = mvc.perform(
                put(BASE_URL + "/add-film/director/{directorId}/film/{filmId}",
                    directorDto.getId(),
                    filmDto.getId()
                )
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        DirectorDto updatedDto = objectMapper.readValue(result, DirectorDto.class);

        assertEquals(updatedDto.getFilmsId().size(), initialFimCount + 1);
    }

    @Test
    void addFilmError() throws Exception {
        mvc.perform(
                put(BASE_URL + "/add-film/director/{directorId}/film/{filmId}", -1, -1)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString();
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
