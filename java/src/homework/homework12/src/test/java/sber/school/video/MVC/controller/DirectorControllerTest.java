package sber.school.video.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import sber.school.video.dto.DirectorDto;
import sber.school.video.service.ModelService;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
public class DirectorControllerTest extends CommonTest {
    @Autowired
    private ModelService modelService;

    @Test
    @Override
    @DisplayName("director:list")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void getAll() throws Exception {
        mvc.perform(
                get("/director/list")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("director/list"))
            .andExpect(model().attributeExists("directors"))
            .andReturn();
    }

    @Test
    @Override
    @DisplayName("director:form")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void form() throws Exception {
        mvc.perform(
                get("/director/form")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("director/form"))
            .andExpect(model().attributeExists("director"))
            .andReturn();
    }

    @Test
    @Override
    @DisplayName("director:create")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void save() throws Exception {
        mvc.perform(
            post("/director/save")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .flashAttr("directorForm", modelService.newDirector())
                .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/director/list"))
            .andExpect(redirectedUrlTemplate("/director/list"));
    }

    @Test
    @Override
    @DisplayName("director:update")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void update() throws Exception {
        DirectorDto directorDto = modelService.createDirector();

        mvc.perform(
                post("/director/save")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .flashAttr("directorForm", directorDto)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/director/list"))
            .andExpect(redirectedUrlTemplate("/director/list"));
    }
}
