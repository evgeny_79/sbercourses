package sber.school.video.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import sber.school.video.dto.UserDto;
import sber.school.video.service.ModelService;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
public class UserControllerTest extends AbstractTest {
    @Autowired
    private ModelService modelService;

    @Test
    @DisplayName("user:list")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void getAll() throws Exception {
        mvc.perform(
                get( "/user/list")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("user/list"))
            .andExpect(model().attributeExists("users"))
            .andExpect(model().attributeExists("roleKeyValue"))
            .andReturn();
    }

    @Test
    @DisplayName("user:registration")
    protected void registration() throws Exception {
        mvc.perform(
                get("/user/registration")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("user/registration"))
            .andReturn();
    }

    @Test
    @DisplayName("user:registration:redirect")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void registrationRedirect() throws Exception {
        mvc.perform(
                get("/user/registration")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/"))
            .andExpect(redirectedUrlTemplate("/"))
            .andReturn();
    }

    @Test
    @DisplayName("user:registration")
    protected void save() throws Exception {
        UserDto userDto = modelService.newUser();

        mvc.perform(
                post("/user/registration")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .flashAttr("userForm", userDto)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/login"))
            .andExpect(redirectedUrlTemplate("/login"))
            .andReturn();
    }

    @Test
    @DisplayName("user:registration:admin")
    protected void saveError() throws Exception {
        UserDto existingUser = modelService.createUser();

        UserDto userDto = modelService.newUser();
        userDto.setLogin("ADMIN");
        userDto.setEmail(existingUser.getEmail());

        mvc.perform(
                post("/user/registration")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .flashAttr("userForm", userDto)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/user/registration"))
            .andExpect(redirectedUrlTemplate("/user/registration"))
            .andReturn();
    }

    @Test
    @DisplayName("user:profile:redirect")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void profileRedirect() throws Exception {
        mvc.perform(
                get("/user/profile")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/"))
            .andExpect(redirectedUrlTemplate("/"))
            .andReturn();
    }

    @Nested
    class TestWithUser{
        @BeforeEach
        public void init() {
            UserDto userDto = modelService.newUser();
            userDto.setLogin("_uniqueTestUser123");

            modelService.getUserService().create(userDto);
        }

        @Test
        @DisplayName("user:profile")
        @WithMockUser(username = "_uniqueTestUser123", password = "admin", roles = "USER")
        protected void profile() throws Exception {
            mvc.perform(
                    get("/user/profile")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("user/profile"))
                .andExpect(model().attributeExists("user"))
                .andReturn();
        }

        @Test
        @DisplayName("user:profile:update")
        @WithMockUser(username = "_uniqueTestUser123", password = "admin", roles = "USER")
        protected void profileUpdate() throws Exception {
            UserDto userDto = modelService.getUserService().getUserByLogin("_uniqueTestUser123");

            mvc.perform(
                    post("/user/profile")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("userForm", userDto)
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/user/profile"))
                .andExpect(redirectedUrlTemplate("/user/profile"))
                .andReturn();
        }

        @Test
        @DisplayName("user:profile:update:error")
        @WithMockUser(username = "_uniqueTestUser123", password = "admin", roles = "USER")
        protected void profileUpdateError() throws Exception {
            UserDto userDto = modelService.getUserService().getUserByLogin("_uniqueTestUser123");

            UserDto crossingEmailUser = modelService.createUser();
            userDto.setEmail(crossingEmailUser.getEmail());

            mvc.perform(
                    post("/user/profile")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("userForm", userDto)
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/user/profile"))
                .andExpect(redirectedUrlTemplate("/user/profile"))
                .andReturn();
        }
    }
}
