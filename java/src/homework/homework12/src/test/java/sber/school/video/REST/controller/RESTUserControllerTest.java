package sber.school.video.REST.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import sber.school.video.dto.FilmDto;
import sber.school.video.dto.LoginDto;
import sber.school.video.dto.UserDto;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@Transactional
public class RESTUserControllerTest extends GenericControllerTest {
    private static final String BASE_URL = "/rest/users";

    @Test
    @Override
    public void getAll() throws Exception {
        String result = super.mvc.perform(
                get(BASE_URL)
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        List<UserDto> directorDtoList = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        directorDtoList.forEach(userDto -> log.info(userDto.getFirstName()));
    }

    @Test
    @Override
    public void getOne() throws Exception {
        UserDto userDto = modelService.createUser();

        String result = super.mvc.perform(
                get(BASE_URL + "/{id}", userDto.getId())
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        UserDto receivedDto = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        assertEquals(receivedDto.getId(), userDto.getId());
    }

    @Test
    @Override
    public void create() throws Exception {
        UserDto userDto = modelService.newUser();

        String result = mvc.perform(
                post(BASE_URL)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(userDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        UserDto created = objectMapper.readValue(result, UserDto.class);

        assertEquals(userDto.getFirstName(), created.getFirstName());
        assertEquals(userDto.getLogin(), created.getLogin());
        assertEquals(userDto.getEmail(), created.getEmail());
    }


    @Test
    @Override
    void deleteOne() throws Exception {
        UserDto userDto = modelService.createUser();

        UserDto checkDto = modelService.getUserService().findOne(userDto.getId());
        assertNotNull(checkDto);

        mvc.perform(
                delete(BASE_URL + "/{id}", userDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        try {
            checkDto = modelService.getUserService().findOne(userDto.getId());
        } catch (NotFoundException e) {
            checkDto = null;
        }

        assertNull(checkDto);
    }

    @Test
    @Override
    void update() throws Exception {
        UserDto userDto = modelService.createUser();
        userDto.setFirstName("UpdatedData");

        String result = mvc.perform(
                put(BASE_URL + "/{id}", userDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(userDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        UserDto updatedDto = objectMapper.readValue(result, UserDto.class);

        assertEquals(updatedDto.getId(), userDto.getId());
        assertEquals(updatedDto.getFirstName(), userDto.getFirstName());
        assertEquals(updatedDto.getLogin(), userDto.getLogin());
        assertEquals(updatedDto.getEmail(), userDto.getEmail());
    }

    @Test
    public void getFilms() throws Exception {
        UserDto userDto = modelService.createUser();

        String result = super.mvc.perform(
                get(BASE_URL + "/get-films/{id}", userDto.getId())
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        List<FilmDto> filmDtoList = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        assertNotNull(filmDtoList);
    }

    @Test
    public void getFilmsError() throws Exception {
        super.mvc.perform(
                get(BASE_URL + "/get-films/{id}", -1)
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void auth() throws Exception {
        UserDto userDto = modelService.createUser();

        LoginDto loginDto = new LoginDto();
        loginDto.setLogin(userDto.getLogin());
        loginDto.setPassword("123");

        super.mvc.perform(
                post(BASE_URL + "/auth")
                    .headers(headers)
                    .content(asJsonString(loginDto))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void authError() throws Exception {
        UserDto userDto = modelService.createUser();

        LoginDto loginDto = new LoginDto();
        loginDto.setLogin(userDto.getLogin());
        loginDto.setPassword("");

        super.mvc.perform(
                post(BASE_URL + "/auth")
                    .headers(headers)
                    .content(asJsonString(loginDto))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().isUnauthorized())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
