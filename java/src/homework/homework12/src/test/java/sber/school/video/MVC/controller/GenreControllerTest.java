package sber.school.video.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import sber.school.video.dto.GenreDto;
import sber.school.video.service.ModelService;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
public class GenreControllerTest extends CommonTest {
    @Autowired
    private ModelService modelService;

    @Test
    @Override
    @DisplayName("genre:list")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void getAll() throws Exception {
        mvc.perform(
                get("/genre/list")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("genre/list"))
            .andExpect(model().attributeExists("genres"))
            .andReturn();
    }

    @Test
    @Override
    @DisplayName("genre:form")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void form() throws Exception {
        mvc.perform(
                get("/genre/form")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("genre/form"))
            .andExpect(model().attributeExists("genre"))
            .andReturn();
    }

    @Test
    @Override
    @DisplayName("genre:create")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void save() throws Exception {
        mvc.perform(
            post("/genre/save")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .flashAttr("genreForm", modelService.newGenre())
                .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/genre/list"))
            .andExpect(redirectedUrlTemplate("/genre/list"));
    }

    @Test
    @Override
    @DisplayName("genre:update")
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    protected void update() throws Exception {
        GenreDto genreDto = modelService.createGenre();

        mvc.perform(
                post("/genre/save")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .flashAttr("genreForm", genreDto)
                    .with(csrf())
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/genre/list"))
            .andExpect(redirectedUrlTemplate("/genre/list"));
    }
}
