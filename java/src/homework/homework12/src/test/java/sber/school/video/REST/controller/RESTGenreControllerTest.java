package sber.school.video.REST.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import sber.school.video.dto.GenreDto;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@Transactional
public class RESTGenreControllerTest extends GenericControllerTest {
    private static final String BASE_URL = "/rest/genres";

    @Test
    @Override
    public void getAll() throws Exception {
        String result = super.mvc.perform(
                get(BASE_URL)
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        List<GenreDto> directorDtoList = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        directorDtoList.forEach(genreDto -> log.info(genreDto.getTitle()));
    }

    @Test
    @Override
    public void getOne() throws Exception {
        GenreDto genreDto = modelService.createGenre();

        String result = super.mvc.perform(
                get(BASE_URL + "/{id}", genreDto.getId())
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        GenreDto receivedDto = objectMapper.readValue(
            result, new TypeReference<>() {}
        );

        assertEquals(receivedDto.getId(), genreDto.getId());
    }

    @Test
    @Override
    public void create() throws Exception {
        GenreDto genreDto = modelService.newGenre();

        String result = mvc.perform(
                post(BASE_URL)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(genreDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        GenreDto created = objectMapper.readValue(result, GenreDto.class);

        assertEquals(genreDto.getTitle(), created.getTitle());
    }


    @Test
    @Override
    void deleteOne() throws Exception {
        GenreDto genreDto = modelService.createGenre();

        GenreDto checkDto = modelService.getGenreService().findOne(genreDto.getId());
        assertNotNull(checkDto);

        mvc.perform(
                delete(BASE_URL + "/{id}", genreDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        try {
            checkDto = modelService.getGenreService().findOne(genreDto.getId());
        } catch (NotFoundException e) {
            checkDto = null;
        }

        assertNull(checkDto);
    }

    @Test
    @Override
    void update() throws Exception {
        GenreDto genreDto = modelService.createGenre();
        genreDto.setTitle("UpdatedData");

        String result = mvc.perform(
                put(BASE_URL + "/{id}", genreDto.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .headers(headers)
                    .content(asJsonString(genreDto))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        GenreDto updatedDto = objectMapper.readValue(result, GenreDto.class);

        assertEquals(updatedDto.getId(), genreDto.getId());
        assertEquals(updatedDto.getTitle(), genreDto.getTitle());
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
