package sber.school.video.REST.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import sber.school.video.config.jwt.JWTTokenUtil;
import sber.school.video.dto.FilmDto;
import sber.school.video.dto.LoginDto;
import sber.school.video.dto.UserDto;
import sber.school.video.model.User;
import sber.school.video.service.UserService;
import sber.school.video.service.userdetails.CustomUserDetailsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
@SecurityRequirement(name = "Bearer Authentication")
public class RESTUserController extends RESTGenericController<User, UserDto> {
    private final UserService service;
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;

    public RESTUserController(
        UserService service,
        CustomUserDetailsService customUserDetailsService,
        JWTTokenUtil jwtTokenUtil
    ) {
        super(service);
        this.service = service;
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @RequestMapping(
        value = "/get-films/{id}",
        method = RequestMethod.GET
    )
    public ResponseEntity<List<FilmDto>> getFilms(@PathVariable(value = "id") Long id) {
        try {
            return ResponseEntity.ok(service.getFilms(id));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDto loginDto) {
        Map<String, Object> response = new HashMap<>();
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDto.getLogin());

        if (!service.checkPassword(loginDto.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\n Неверный пароль!");
        }

        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("authorities", foundUser.getAuthorities());

        return ResponseEntity.ok().body(response);
    }
}
