package sber.school.video.repository;

import org.springframework.stereotype.Repository;
import sber.school.video.model.Genre;

@Repository
public interface GenreRepository extends GenericRepository<Genre> {
}
