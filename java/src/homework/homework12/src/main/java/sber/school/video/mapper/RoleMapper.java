package sber.school.video.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.video.dto.RoleDto;
import sber.school.video.model.Role;

@Component
public class RoleMapper extends GenericMapper<Role, RoleDto> {
    protected RoleMapper(ModelMapper modelMapper) {
        super(modelMapper, Role.class, RoleDto.class);
    }
}
