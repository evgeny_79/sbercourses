package sber.school.video.repository;

import org.springframework.stereotype.Repository;
import sber.school.video.model.Role;

@Repository
public interface RoleRepository extends GenericRepository<Role> {
    Role findByTitle(String title);
}
