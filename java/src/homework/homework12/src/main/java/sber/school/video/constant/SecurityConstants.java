package sber.school.video.constant;

import java.util.List;

public interface SecurityConstants {
    List<String> STATIC_WHITE_LIST = List.of(
        "/",
        "/error",
        "/js/**",
        "/css/**",
        "/img/**",
        "/swagger-ui/**",
        "/v3/api-docs/**"
    );

    // Доступно всем
    List<String> MVC_WHITE_LIST = List.of(
        "/user/registration"
    );

    // Доступно всем кто прошел авторизацию
    List<String> MVC_AUTHENTICATED_LIST = List.of(
        "/film/list",
        "/genre/list",
        "/director/list",
        "/user/profile",
        "/order/create/{filmId:[\\d]+}",
        "/order/create",
        "/order/list"
    );

    // Доступно всем сотрудникам
    List<String> MVC_STAFF_LIST = List.of(
        "/film/form",
        "/film/form/{id:[\\d]+}",
        "/film/save",

        "/genre/form",
        "/genre/form/{id:[\\d]+}",
        "/genre/save",

        "/director/form",
        "/director/form/{id:[\\d]+}",
        "/director/save",

        "/user/list"
    );

    // Доступно всем
    List<String> REST_WHITE_LIST = List.of(
        "/rest/users/auth"
    );

    // Доступно всем кто прошел авторизацию
    List<String> REST_ACTION_LIST = List.of(
        "/rest/**"
    );
}
