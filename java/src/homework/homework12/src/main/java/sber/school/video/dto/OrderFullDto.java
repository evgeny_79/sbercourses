package sber.school.video.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderFullDto extends GenericDto {
    private LocalDate rentDate;
    private Integer rentPeriod;
    private Boolean isPurchased;
    private UserDto userDto;
    private FilmDto filmDto;
}
