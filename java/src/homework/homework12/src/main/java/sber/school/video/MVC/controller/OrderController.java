package sber.school.video.MVC.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import sber.school.video.constant.UserRolesConstants;
import sber.school.video.dto.FilmDto;
import sber.school.video.dto.OrderDto;
import sber.school.video.dto.UserDto;
import sber.school.video.service.*;

import java.time.LocalDate;

@Controller
@RequestMapping("/order")
public class OrderController {
    private final OrderService service;
    private final FilmService filmService;
    private final UserService userService;

    public OrderController(
        OrderService service,
        FilmService filmService,
        UserService userService
    ) {
        this.service = service;
        this.filmService = filmService;
        this.userService = userService;
    }

    @GetMapping("/list")
    public String getAll(
        Model model,
        Authentication authentication
    ) {
        boolean isStaff = authentication.getAuthorities().contains(
            new SimpleGrantedAuthority("ROLE_" + UserRolesConstants.STAFF)
        );

        if (isStaff) {
            model.addAttribute("orders", service.getUsersOrders());
        } else {
            UserDto userDto = userService.getUserByLogin(authentication.getName());
            model.addAttribute("orders", service.getUserOrders(userDto.getId()));
        }

        return "order/list";
    }

    @GetMapping("/create/{filmId}")
    public String create(
        @PathVariable(value = "filmId") Long filmId,
        Model model
    ) {
        FilmDto filmDto;

        try {
            filmDto = filmService.findOne(filmId);
        } catch (NotFoundException e) {
            return "redirect:/film/list";
        }

        OrderDto orderDto = new OrderDto();
        orderDto.setFilmId(filmDto.getId());

        model.addAttribute("film", filmDto);
        model.addAttribute("order", new OrderDto());

        return "order/create";
    }

    @PostMapping("/create")
    public String create(
        @ModelAttribute("orderForm") OrderDto orderDto,
        Authentication authentication
    ) {
        UserDto userDto = userService.getUserByLogin(authentication.getName());

        orderDto.setUserId(userDto.getId());
        orderDto.setRentDate(LocalDate.now());

        if (orderDto.getIsPurchased() == null) {
            orderDto.setIsPurchased(false);
        }

        if (orderDto.getIsPurchased()) {
            orderDto.setRentPeriod(0);
        }

        service.create(orderDto);

        return "redirect:/order/list";
    }
}
