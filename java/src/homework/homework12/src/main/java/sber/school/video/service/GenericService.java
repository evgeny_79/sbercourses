package sber.school.video.service;

import org.webjars.NotFoundException;
import sber.school.video.dto.GenericDto;
import sber.school.video.mapper.GenericMapper;
import sber.school.video.model.GenericModel;
import sber.school.video.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;

public abstract class GenericService<T extends GenericModel, N extends GenericDto> {
    private final GenericRepository<T> repository;
    private final GenericMapper<T, N> mapper;

    protected GenericService(
        GenericRepository<T> repository,
        GenericMapper<T, N> mapper
    ) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> findAll() {
        return mapper.toDtoList(repository.findAll());
    }

    public N findOne(Long id) {
        return mapper.toDto(
            repository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Запись с таким id не найдена"))
        );
    }

    public N create(N object) {
        T entity = mapper.toEntity(object);

        entity.setCreatedBy("ADMIN");
        entity.setCreatedWhen(LocalDateTime.now());

        return mapper.toDto(
            repository.save(entity)
        );
    }

    public N update(N object) {
        return mapper.toDto(repository.save(mapper.toEntity(object)));
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public boolean existsById(Long id) {
        if (id == null) {
            return false;
        }

        return repository.existsById(id);
    }
}
