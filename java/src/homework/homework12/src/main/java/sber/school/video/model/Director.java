package sber.school.video.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "directors")
@SequenceGenerator(name = "default_gen", sequenceName = "directors_seq", allocationSize = 1)
public class Director extends GenericModel {
    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "position", nullable = false)
    private String position;

    @ManyToMany(
        fetch = FetchType.EAGER,
        cascade = CascadeType.REMOVE
    )
    @JoinTable(
        name = "film_directors",
        joinColumns = @JoinColumn(name = "director_id"),
        foreignKey = @ForeignKey(name = "FK_DIRECTOR_FILMS"),
        inverseJoinColumns = @JoinColumn(name = "film_id"),
        inverseForeignKey = @ForeignKey(name = "FK_FILM_DIRECTORS")
    )
    private Set<Film> films;
}
