package sber.school.video.config;

import java.util.Arrays;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import sber.school.video.config.jwt.JWTTokenFilter;
import sber.school.video.service.userdetails.CustomUserDetailsService;

import static sber.school.video.constant.SecurityConstants.*;
import static sber.school.video.constant.UserRolesConstants.*;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenFilter jwtTokenFilter;

    public WebSecurityConfig(
        BCryptPasswordEncoder bCryptPasswordEncoder,
        CustomUserDetailsService customUserDetailsService,
        JWTTokenFilter jwtTokenFilter
    ) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenFilter = jwtTokenFilter;
    }

    @Bean
    public HttpFirewall httpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowedHttpMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));

        return firewall;
    }

    @Bean
    @Order(1)
    public SecurityFilterChain restSecurityFilterChain(HttpSecurity http) throws Exception {
        http.securityMatcher("/rest/**")
            .authorizeHttpRequests((requests) ->
                requests
                    .requestMatchers(REST_WHITE_LIST.toArray(String[]::new)).permitAll()
                    .requestMatchers(HttpMethod.GET, REST_ACTION_LIST.toArray(String[]::new)).authenticated()
                    .requestMatchers(HttpMethod.POST, REST_ACTION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, STAFF)
                    .requestMatchers(HttpMethod.DELETE, REST_ACTION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, STAFF)
                    .requestMatchers(HttpMethod.PUT, REST_ACTION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, STAFF)
            )
            .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
            .cors(AbstractHttpConfigurer::disable)
            .csrf(AbstractHttpConfigurer::disable)
            .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
            .userDetailsService(customUserDetailsService)
            .exceptionHandling()
            .authenticationEntryPoint((request, response, authException) -> {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
            });

        return http.build();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
            .csrf(csrf -> csrf
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
            )
            .authorizeHttpRequests((requests) ->
                requests
                    .requestMatchers(STATIC_WHITE_LIST.toArray(String[]::new)).permitAll()

                    .requestMatchers(MVC_WHITE_LIST.toArray(String[]::new)).permitAll()
                    .requestMatchers(MVC_AUTHENTICATED_LIST.toArray(String[]::new)).authenticated()
                    .requestMatchers(MVC_STAFF_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, STAFF)

                    .requestMatchers(MVC_WHITE_LIST.toArray(String[]::new)).permitAll()
                    .requestMatchers(MVC_AUTHENTICATED_LIST.toArray(String[]::new)).authenticated()
                    .requestMatchers(MVC_STAFF_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, STAFF)
            )
            .formLogin((form) -> form
                .loginPage("/login")
                .defaultSuccessUrl("/")
                .permitAll()
            )
            .logout((logout) -> logout
                .logoutSuccessUrl("/login")
                .logoutUrl("/logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .permitAll()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            );

        return http.build();
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }
}
