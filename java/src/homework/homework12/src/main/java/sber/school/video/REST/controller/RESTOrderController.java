package sber.school.video.REST.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sber.school.video.dto.OrderDto;
import sber.school.video.model.Order;
import sber.school.video.service.OrderService;

@RestController
@RequestMapping("/rest/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
@SecurityRequirement(name = "Bearer Authentication")
public class RESTOrderController extends RESTGenericController<Order, OrderDto> {
    public RESTOrderController(OrderService service) {
        super(service);
    }
}
