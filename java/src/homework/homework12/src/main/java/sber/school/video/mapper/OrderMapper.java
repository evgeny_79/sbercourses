package sber.school.video.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.video.dto.OrderDto;
import sber.school.video.model.Order;
import sber.school.video.repository.FilmRepository;
import sber.school.video.repository.UserRepository;

import java.util.Objects;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDto> {
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    protected OrderMapper(
        ModelMapper modelMapper,
        FilmRepository filmRepository,
        UserRepository userRepository
    ) {
        super(modelMapper, Order.class, OrderDto.class);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper
            .createTypeMap(Order.class, OrderDto.class)
            .addMappings(m -> m.skip(OrderDto::setFilmId))
            .addMappings(m -> m.skip(OrderDto::setUserId))
            .setPostConverter(toDtoConverter());

        modelMapper
            .createTypeMap(OrderDto.class, Order.class)
            .addMappings(m -> m.skip(Order::setFilm))
            .addMappings(m -> m.skip(Order::setUser))
            .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDto source, Order destination) {
        if (!Objects.isNull(source.getFilmId())) {
            destination.setFilm(
                filmRepository.findById(source.getFilmId()).orElseThrow()
            );
        }

        if (!Objects.isNull(source.getUserId())) {
            destination.setUser(
                userRepository.findById(source.getUserId()).orElseThrow()
            );
        }
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDto destination) {
        destination.setFilmId(source.getFilm().getId());
        destination.setUserId(source.getUser().getId());
    }
}
