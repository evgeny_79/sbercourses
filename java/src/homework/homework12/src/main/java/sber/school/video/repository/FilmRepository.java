package sber.school.video.repository;

import org.springframework.stereotype.Repository;
import sber.school.video.model.Film;

@Repository
public interface FilmRepository extends GenericRepository<Film> {
}
