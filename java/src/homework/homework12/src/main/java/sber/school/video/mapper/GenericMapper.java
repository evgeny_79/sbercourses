package sber.school.video.mapper;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.video.dto.GenericDto;
import sber.school.video.model.GenericModel;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public abstract class GenericMapper<E extends GenericModel, D extends GenericDto> implements Mapper<E, D> {
    protected final ModelMapper modelMapper;
    private final Class<E> entityClass;
    private final Class<D> dtoClass;

    protected GenericMapper(
        ModelMapper modelMapper,
        Class<E> entityClass,
        Class<D> dtoClass
    ) {
        this.modelMapper = modelMapper;
        this.entityClass = entityClass;
        this.dtoClass    = dtoClass;
    }

    @Override
    public E toEntity(D dto) {
        return Objects.isNull(dto)
            ? null
            : modelMapper.map(dto, entityClass);
    }

    @Override
    public List<E> toEntityList(List<D> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    public Set<E> toEntitySet(Set<D> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toSet());
    }

    @Override
    public D toDto(E entity) {
        return Objects.isNull(entity)
            ? null
            : modelMapper.map(entity, dtoClass);
    }

    @Override
    public List<D> toDtoList(List<E> entityList) {
        return entityList.stream().map(this::toDto).toList();
    }

    public Set<D> toDtoSet(Set<E> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toSet());
    }

    public Converter<D, E> toEntityConverter() {
        return context -> {
            D source = context.getSource();
            E destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    public Converter<E, D> toDtoConverter() {
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    protected void mapSpecificFields(D source, E destination) {
    }

    protected void mapSpecificFields(E source, D destination) {
    }
}
