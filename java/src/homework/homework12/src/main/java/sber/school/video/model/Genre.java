package sber.school.video.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "genres")
@SequenceGenerator(name = "default_gen", sequenceName = "genres_seq", allocationSize = 1)
public class Genre extends GenericModel {
    @Column(name = "title")
    private String title;
}
