package sber.school.video.repository;

import org.springframework.stereotype.Repository;
import sber.school.video.model.Order;

import java.util.List;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
    List<Order> findAllByUserIdOrderByRentDateDesc(Long userId);
    List<Order> findAllByOrderByRentDateDesc();
}
