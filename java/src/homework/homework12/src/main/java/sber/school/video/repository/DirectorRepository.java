package sber.school.video.repository;

import org.springframework.stereotype.Repository;
import sber.school.video.model.Director;

import java.util.List;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
    List<Director> findByPosition(String position);
}
