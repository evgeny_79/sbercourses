package sber.school.video.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;
import sber.school.video.dto.FilmDto;
import sber.school.video.model.Film;
import sber.school.video.service.FilmService;

@RestController
@RequestMapping("/rest/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами")
@SecurityRequirement(name = "Bearer Authentication")
public class RESTFilmController extends RESTGenericController<Film, FilmDto> {
    private final FilmService service;

    public RESTFilmController(FilmService service) {
        super(service);
        this.service = service;
    }

    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
    @RequestMapping(
        value = "add-director/film/{filmId}/director/{directorId}",
        method = RequestMethod.PUT
    )
    public ResponseEntity<FilmDto> addDirector(
        @PathVariable(value = "filmId") Long filmId,
        @PathVariable(value = "directorId") Long directorId
    ) {
        try {
            return ResponseEntity.ok(service.addDirector(filmId, directorId));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
