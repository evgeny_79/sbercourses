package sber.school.video.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;

@Controller
public class MainController {
    @GetMapping("/")
    public String getAll() {
        return "main/main";
    }
}
