package sber.school.video.MVC.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import sber.school.video.constant.UserRolesConstants;
import sber.school.video.dto.UserDto;
import sber.school.video.service.RoleService;
import sber.school.video.service.UserService;

import static sber.school.video.constant.UserRolesConstants.ADMIN;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService service;
    private final RoleService roleService;

    public UserController(
        UserService service,
        RoleService roleService
    ) {
        this.service = service;
        this.roleService = roleService;
    }

    @GetMapping("/list")
    public String getAll(Model model) {
        model.addAttribute("users", service.findAll());
        model.addAttribute("roleKeyValue", roleService.getKeyValueList());

        return "user/list";
    }

    @GetMapping("/registration")
    public String registration() {
        if (SecurityContextHolder.getContext().getAuthentication() != null &&
            SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
            !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)
        ) {
            return "redirect:/";
        }

        return "user/registration";
    }

    @PostMapping("/registration")
    public String registration(
        @ModelAttribute("userForm") UserDto userDto,
        BindingResult bindingResult
    ) {
        if (userDto.getLogin().equals(ADMIN) || service.getUserByLogin(userDto.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже существует");
        }

        if (service.getUserByEmail(userDto.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такая почта уже существует");
        }

        if (bindingResult.hasErrors()) {
            return "redirect:/user/registration";
        }

        userDto.setRoleId(roleService.findRoleIdByTitle(
            UserRolesConstants.USER
        ));

        service.create(userDto);

        return "redirect:/login";
    }

    @GetMapping("/profile")
    public String profile(
        Model model,
        Authentication authentication
    ) {
        UserDto userDto = service.getUserByLogin(authentication.getName());

        if (userDto == null) {
            return "redirect:/";
        }

        model.addAttribute("user", userDto);

        return "user/profile";
    }

    @PostMapping("/profile")
    public String profile(
        @ModelAttribute("userForm") UserDto userDto,
        Authentication authentication,
        BindingResult bindingResult
    ) {
        UserDto userByEmail = service.getUserByEmail(userDto.getEmail());
        UserDto currentUser = service.getUserByLogin(authentication.getName());

        // Пользователь с такой почтой уже есть и это не текущий пользователь
        if (userByEmail != null && !userByEmail.getLogin().equals(currentUser.getLogin())) {
            bindingResult.rejectValue("email", "error.email", "Такая почта уже существует");
        }

        if (!bindingResult.hasErrors()) {
            service.updateProfile(authentication, userDto);
        }

        return "redirect:/user/profile";
    }
}
