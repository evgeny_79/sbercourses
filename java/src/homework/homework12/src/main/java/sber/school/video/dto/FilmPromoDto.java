package sber.school.video.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmPromoDto extends FilmDto {
    private Set<GenreDto> genres;
    private Set<DirectorDto> directors;
}
