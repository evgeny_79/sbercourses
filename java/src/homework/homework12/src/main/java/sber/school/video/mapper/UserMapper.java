package sber.school.video.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import sber.school.video.dto.UserDto;
import sber.school.video.model.User;
import sber.school.video.model.GenericModel;
import sber.school.video.repository.OrderRepository;
import sber.school.video.repository.RoleRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDto> {
    private final OrderRepository orderRepository;
    private final RoleRepository roleRepository;

    protected UserMapper(
        ModelMapper modelMapper,
        OrderRepository orderRepository,
        RoleRepository roleRepository) {
        super(modelMapper, User.class, UserDto.class);
        this.orderRepository = orderRepository;
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper
            .createTypeMap(User.class, UserDto.class)
            .addMappings(m -> m.skip(UserDto::setRoleId))
            .addMappings(m -> m.skip(UserDto::setOrdersId))
            .setPostConverter(toDtoConverter());

        modelMapper
            .createTypeMap(UserDto.class, User.class)
            .addMappings(m -> m.skip(User::setRole))
            .addMappings(m -> m.skip(User::setOrders))
            .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDto source, User destination) {
        destination.setRole(
            roleRepository
                .findById(source.getRoleId())
                .orElseThrow(() -> new NotFoundException("Роль не найдена"))
        );

        if (!Objects.isNull(source.getOrdersId())) {
            destination.setOrders(
                new HashSet<>(
                    orderRepository.findAllById(source.getOrdersId())
                )
            );
        }
    }

    @Override
    protected void mapSpecificFields(User source, UserDto destination) {
        destination.setRoleId(source.getRole().getId());
        destination.setOrdersId(getOrdersId(source));
    }

    protected Set<Long> getOrdersId(User entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
            ? null
            : entity.getOrders()
            .stream()
            .map(GenericModel::getId)
            .collect(Collectors.toSet());
    }
}
