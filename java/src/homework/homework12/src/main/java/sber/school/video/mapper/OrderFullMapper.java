package sber.school.video.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.video.dto.OrderFullDto;
import sber.school.video.model.Order;

import java.util.Objects;

@Component
public class OrderFullMapper extends GenericMapper<Order, OrderFullDto> {
    private final FilmMapper filmMapper;
    private final UserMapper userMapper;

    protected OrderFullMapper(
        ModelMapper modelMapper,
        FilmMapper filmMapper,
        UserMapper userMapper
    ) {
        super(modelMapper, Order.class, OrderFullDto.class);
        this.filmMapper = filmMapper;
        this.userMapper = userMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper
            .createTypeMap(Order.class, OrderFullDto.class)
            .addMappings(m -> m.skip(OrderFullDto::setFilmDto))
            .addMappings(m -> m.skip(OrderFullDto::setUserDto))
            .setPostConverter(toDtoConverter());

        modelMapper
            .createTypeMap(OrderFullDto.class, Order.class)
            .addMappings(m -> m.skip(Order::setFilm))
            .addMappings(m -> m.skip(Order::setUser))
            .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderFullDto source, Order destination) {
        if (!Objects.isNull(source.getFilmDto())) {
            destination.setFilm(
                filmMapper.toEntity(source.getFilmDto())
            );
        }

        if (!Objects.isNull(source.getUserDto())) {
            destination.setUser(
                userMapper.toEntity(source.getUserDto())
            );
        }
    }

    @Override
    protected void mapSpecificFields(Order source, OrderFullDto destination) {
        destination.setFilmDto(filmMapper.toDto(source.getFilm()));
        destination.setUserDto(userMapper.toDto(source.getUser()));
    }
}
