package sber.school.video.constant;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String STAFF = "STAFF";
    String USER = "USER";
}
