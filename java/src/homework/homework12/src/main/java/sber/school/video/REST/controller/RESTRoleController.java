package sber.school.video.REST.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sber.school.video.dto.RoleDto;
import sber.school.video.model.Role;
import sber.school.video.service.RoleService;

@RestController
@RequestMapping("/rest/roles")
@Tag(name = "Роли", description = "Контроллер для работы с ролями пользователей")
@SecurityRequirement(name = "Bearer Authentication")
public class RESTRoleController extends RESTGenericController<Role, RoleDto> {
    public RESTRoleController(RoleService service) {
        super(service);
    }
}
