package sber.school.video.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sber.school.video.dto.DirectorDto;
import sber.school.video.service.DirectorService;

@Controller
@RequestMapping("/director")
public class DirectorController {
    private final DirectorService directorService;

    public DirectorController(
        DirectorService directorService
    ) {
        this.directorService = directorService;
    }

    @GetMapping("/list")
    public String getAll(Model model) {
        model.addAttribute("directors", directorService.findAll());
        return "director/list";
    }

    @GetMapping(value = {"/form", "/form/{id}"})
    public String form(Model model, @PathVariable(required = false) Long id) {
        model.addAttribute("director", id == null ? new DirectorDto() : directorService.findOne(id));

        return "director/form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("directorForm") DirectorDto directorDto) {
        if (directorDto.getId() == null) {
            directorService.create(directorDto);
        } else {
            directorDto.setFilmsId(
                directorService.findOne(directorDto.getId()).getFilmsId()
            );

            directorService.update(directorDto);
        }

        return "redirect:/director/list";
    }
}
