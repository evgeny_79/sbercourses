package sber.school.video.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "orders")
@SequenceGenerator(name = "default_gen", sequenceName = "orders_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "jsonId")
public class Order extends GenericModel {
    @Column(name = "rent_date", nullable = false)
    private LocalDate rentDate;

    @Column(name = "rent_period", nullable = false)
    private Integer rentPeriod;

    @Column(name = "is_purchased", nullable = false)
    private Boolean isPurchased;

    @ManyToOne(
        fetch = FetchType.EAGER
    )
    @JoinColumn(
        name = "user_id",
        nullable = false,
        foreignKey = @ForeignKey(name = "FK_ORDER_USER")
    )
    private User user;

    @ManyToOne(
        fetch = FetchType.EAGER
    )
    @JoinColumn(
        name = "film_id",
        nullable = false,
        foreignKey = @ForeignKey(name = "FK_ORDER_FILM")
    )
    private Film film;
}
