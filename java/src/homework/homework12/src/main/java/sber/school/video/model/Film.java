package sber.school.video.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "films")
@SequenceGenerator(name = "default_gen", sequenceName = "films_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "jsonId")
public class Film extends GenericModel {
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "premier_year", nullable = false)
    private Integer premierYear;

    @Column(name = "country", nullable = false)
    private String country;

    @ManyToMany(
        fetch = FetchType.LAZY
    )
    @JoinTable(
        name = "film_genres",
        joinColumns = @JoinColumn(name = "film_id"),
        foreignKey = @ForeignKey(name = "FK_FILM_GENRES"),
        inverseJoinColumns = @JoinColumn(name = "genre_id"),
        inverseForeignKey = @ForeignKey(name = "FK_GENRE_FILMS")
    )
    @ToString.Exclude
    private Set<Genre> genres;

    @ManyToMany(
        fetch = FetchType.LAZY
    )
    @JoinTable(
        name = "film_directors",
        joinColumns = @JoinColumn(name = "film_id"),
        foreignKey = @ForeignKey(name = "FK_FILM_DIRECTORS"),
        inverseJoinColumns = @JoinColumn(name = "director_id"),
        inverseForeignKey = @ForeignKey(name = "FK_DIRECTOR_FILMS")
    )
    @ToString.Exclude
    private Set<Director> directors;

    @OneToMany(
        mappedBy = "film",
        fetch = FetchType.LAZY,
        cascade = CascadeType.REMOVE
    )
    @ToString.Exclude
    private Set<Order> orders;

    public void addGenre(Genre genre) {
        if (this.genres == null) {
            this.genres = new HashSet<>();
        }

        this.genres.add(genre);
    }

    public void addDirector(Director director) {
        if (this.directors == null) {
            this.directors = new HashSet<>();
        }

        this.directors.add(director);
    }
}
