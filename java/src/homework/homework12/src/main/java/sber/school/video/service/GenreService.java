package sber.school.video.service;

import org.springframework.stereotype.Service;
import sber.school.video.dto.GenreDto;
import sber.school.video.mapper.GenreMapper;
import sber.school.video.model.Genre;
import sber.school.video.repository.GenreRepository;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class GenreService extends GenericService<Genre, GenreDto> {
    private final GenreRepository repository;

    protected GenreService(
        GenreRepository repository,
        GenreMapper mapper
    ) {
        super(repository, mapper);
        this.repository = repository;
    }

    public Map<Long, String> getKeyValueList() {
        return repository.findAll().stream().collect(Collectors.toMap(
            Genre::getId, Genre::getTitle
        ));
    }
}
