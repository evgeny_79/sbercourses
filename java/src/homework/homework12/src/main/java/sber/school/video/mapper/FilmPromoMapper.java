package sber.school.video.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.video.dto.FilmPromoDto;
import sber.school.video.dto.FilmDto;
import sber.school.video.model.Film;

import java.util.HashSet;
import java.util.Objects;

@Component
public class FilmPromoMapper extends GenericMapper<Film, FilmPromoDto> {
    private final GenreMapper genreMapper;
    private final DirectorMapper directorMapper;

    protected FilmPromoMapper(
        ModelMapper modelMapper,
        GenreMapper genreMapper,
        DirectorMapper directorMapper
    ) {
        super(modelMapper, Film.class, FilmPromoDto.class);
        this.genreMapper    = genreMapper;
        this.directorMapper = directorMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper
            .createTypeMap(Film.class, FilmPromoDto.class)
            .addMappings(m -> m.skip(FilmDto::setGenresId))
            .addMappings(m -> m.skip(FilmDto::setDirectorsId))
            .addMappings(m -> m.skip(FilmDto::setOrdersId))
            .setPostConverter(toDtoConverter());

        modelMapper
            .createTypeMap(FilmPromoDto.class, Film.class)
            .addMappings(m -> m.skip(Film::setGenres))
            .addMappings(m -> m.skip(Film::setDirectors))
            .addMappings(m -> m.skip(Film::setOrders))
            .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmPromoDto source, Film destination) {
        if (!Objects.isNull(source.getGenresId())) {
            destination.setGenres(
                new HashSet<>(
                    genreMapper.toEntitySet(source.getGenres())
                )
            );
        }

        if (!Objects.isNull(source.getDirectorsId())) {
            destination.setDirectors(
                new HashSet<>(
                    directorMapper.toEntitySet(source.getDirectors())
                )
            );
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmPromoDto destination) {
        destination.setGenres(genreMapper.toDtoSet(source.getGenres()));
        destination.setDirectors(directorMapper.toDtoSet(source.getDirectors()));
    }
}
