package sber.school.video.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDto extends GenericDto {
    private String title;
    private Integer premierYear;
    private String country;
    private Set<Long> genresId;
    private Set<Long> directorsId;
    private Set<Long> ordersId;

    public FilmDto(Set<Long> genresId, Set<Long> directorsId) {
        this.genresId = genresId;
        this.directorsId = directorsId;
    }
}
