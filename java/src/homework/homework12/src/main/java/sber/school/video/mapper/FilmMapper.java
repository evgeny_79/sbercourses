package sber.school.video.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.video.dto.FilmDto;
import sber.school.video.model.Film;
import sber.school.video.model.GenericModel;
import sber.school.video.repository.DirectorRepository;
import sber.school.video.repository.GenreRepository;
import sber.school.video.repository.OrderRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDto> {
    private final GenreRepository genreRepository;
    private final DirectorRepository directorRepository;
    private final OrderRepository orderRepository;

    protected FilmMapper(
        ModelMapper modelMapper,
        GenreRepository genreRepository,
        DirectorRepository directorRepository,
        OrderRepository orderRepository
    ) {
        super(modelMapper, Film.class, FilmDto.class);
        this.genreRepository = genreRepository;
        this.directorRepository = directorRepository;
        this.orderRepository = orderRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper
            .createTypeMap(Film.class, FilmDto.class)
            .addMappings(m -> m.skip(FilmDto::setGenresId))
            .addMappings(m -> m.skip(FilmDto::setDirectorsId))
            .addMappings(m -> m.skip(FilmDto::setOrdersId))
            .setPostConverter(toDtoConverter());

        modelMapper
            .createTypeMap(FilmDto.class, Film.class)
            .addMappings(m -> m.skip(Film::setGenres))
            .addMappings(m -> m.skip(Film::setDirectors))
            .addMappings(m -> m.skip(Film::setOrders))
            .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDto source, Film destination) {
        if (!Objects.isNull(source.getGenresId())) {
            destination.setGenres(
                new HashSet<>(
                    genreRepository.findAllById(source.getGenresId())
                )
            );
        }

        if (!Objects.isNull(source.getDirectorsId())) {
            destination.setDirectors(
                new HashSet<>(
                    directorRepository.findAllById(source.getDirectorsId())
                )
            );
        }

        if (!Objects.isNull(source.getOrdersId())) {
            destination.setOrders(
                new HashSet<>(
                    orderRepository.findAllById(source.getOrdersId())
                )
            );
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDto destination) {
        destination.setGenresId(getGenresId(source));
        destination.setDirectorsId(getFilmsId(source));
        destination.setOrdersId(getOrdersId(source));
    }

    protected Set<Long> getGenresId(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getGenres())
            ? null
            : entity.getGenres()
            .stream()
            .map(GenericModel::getId)
            .collect(Collectors.toSet());
    }

    protected Set<Long> getFilmsId(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getDirectors())
            ? null
            : entity.getDirectors()
            .stream()
            .map(GenericModel::getId)
            .collect(Collectors.toSet());
    }

    protected Set<Long> getOrdersId(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
            ? null
            : entity.getOrders()
            .stream()
            .map(GenericModel::getId)
            .collect(Collectors.toSet());
    }
}
