package sber.school.video.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sber.school.video.dto.FilmDto;
import sber.school.video.service.DirectorService;
import sber.school.video.service.FilmService;
import sber.school.video.service.GenreService;

import java.util.HashSet;

@Controller
@RequestMapping("/film")
public class FilmController {
    private final FilmService filmService;
    private final GenreService genreService;
    private final DirectorService directorService;

    public FilmController(
        FilmService filmService,
        GenreService genreService,
        DirectorService directorService
    ) {
        this.filmService = filmService;
        this.genreService = genreService;
        this.directorService = directorService;
    }

    @GetMapping("/list")
    public String getAll(Model model) {
        model.addAttribute("films", filmService.getPromoList());
        return "film/list";
    }

    @GetMapping(value = {"/form", "/form/{id}"})
    public String form(Model model, @PathVariable(required = false) Long id) {
        model.addAttribute("film", id == null ? new FilmDto(new HashSet<>(), new HashSet<>()) : filmService.findOne(id));
        model.addAttribute("genreList", genreService.getKeyValueList());
        model.addAttribute("directorList", directorService.getKeyValueList());

        return "film/form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("filmForm") FilmDto filmDto) {
        if (filmDto.getId() == null) {
            filmService.update(filmDto);
        } else {
            filmService.create(filmDto);
        }

        return "redirect:/film/list";
    }
}
