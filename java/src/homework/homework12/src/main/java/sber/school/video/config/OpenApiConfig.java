package sber.school.video.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SecurityScheme(
    name = "Bearer Authentication",
    type = SecuritySchemeType.HTTP,
    bearerFormat = "JWT",
    scheme = "bearer"
)
@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI videoProject() {
        return new OpenAPI()
                .info(
                    new Info()
                        .title("Онлайн видеотека")
                        .description("")
                        .version("v0")
                        .license(new License().name("Apache 2.0").url("https://springdoc.org"))
                        .contact(
                            new Contact().name("Developer").email("dev@dev.dev").url("dev.net")
                        )
                );
    }
}
