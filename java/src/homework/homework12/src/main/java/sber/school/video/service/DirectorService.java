package sber.school.video.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import sber.school.video.dto.DirectorDto;
import sber.school.video.mapper.DirectorMapper;
import sber.school.video.model.Director;
import sber.school.video.model.Film;
import sber.school.video.repository.DirectorRepository;
import sber.school.video.repository.FilmRepository;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DirectorService extends GenericService<Director, DirectorDto> {
    private final DirectorRepository repository;
    private final DirectorMapper mapper;
    private final FilmRepository filmRepository;

    protected DirectorService(
        DirectorRepository repository,
        DirectorMapper mapper,
        FilmRepository filmRepository
    ) {
        super(repository, mapper);
        this.repository = repository;
        this.mapper = mapper;
        this.filmRepository = filmRepository;
    }


    public DirectorDto addFilm(Long directorId, Long filmId) {
        Film film = filmRepository
            .findById(filmId)
            .orElseThrow(() -> new NotFoundException("Фильм не найден"));

        Director director = repository
            .findById(directorId)
            .orElseThrow(() -> new NotFoundException("Режиссер не найден"));

        director.getFilms().add(film);

        return mapper.toDto(repository.save(director));
    }

    public Map<Long, String> getKeyValueList() {
        return repository.findAll().stream().collect(Collectors.toMap(
            Director::getId, Director::getFullName
        ));
    }
}
