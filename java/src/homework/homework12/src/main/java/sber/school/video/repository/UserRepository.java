package sber.school.video.repository;

import org.springframework.stereotype.Repository;
import sber.school.video.model.User;

@Repository
public interface UserRepository extends GenericRepository<User> {
    User findUserByLogin(String login);
    User findUserByEmail(String email);
}
