package sber.school.video.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import sber.school.video.dto.FilmDto;
import sber.school.video.dto.UserDto;
import sber.school.video.mapper.FilmMapper;
import sber.school.video.mapper.UserMapper;
import sber.school.video.model.Order;
import sber.school.video.model.User;
import sber.school.video.repository.UserRepository;

import java.util.List;

@Service
public class UserService extends GenericService<User, UserDto> {
    private final UserRepository repository;
    private final UserMapper mapper;
    private final FilmMapper filmMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    protected UserService(
        UserRepository repository,
        UserMapper mapper,
        FilmMapper filmMapper,
        BCryptPasswordEncoder bCryptPasswordEncoder
    ) {
        super(repository, mapper);
        this.repository = repository;
        this.mapper = mapper;
        this.filmMapper = filmMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<FilmDto> getFilms(Long id) throws NotFoundException {
        User user = repository
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Пользователь не найден"));

        return filmMapper.toDtoList(
            user.getOrders().stream().map(Order::getFilm).toList()
        );
    }

    @Override
    public UserDto create(UserDto userDto) {
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        return super.create(userDto);
    }

    public void updateProfile(
        Authentication authentication,
        UserDto userDto
    ) {
        UserDto currentUserDto = getUserByLogin(authentication.getName());

        if (userDto.getPassword() != null && userDto.getPassword().length() > 0) {
            userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        } else {
            userDto.setPassword(currentUserDto.getPassword());
        }

        // неизменяемые данные
        userDto.setId(currentUserDto.getId());
        userDto.setLogin(currentUserDto.getLogin());
        userDto.setRoleId(currentUserDto.getRoleId());

        super.update(userDto);
    }

    public UserDto getUserByLogin(String login) {
        return mapper.toDto(repository.findUserByLogin(login));
    }

    public UserDto getUserByEmail(String email) {
        return mapper.toDto(repository.findUserByEmail(email));
    }

    public boolean checkPassword(String password, UserDetails foundUser) {
        return bCryptPasswordEncoder.matches(password, foundUser.getPassword());
    }
}
