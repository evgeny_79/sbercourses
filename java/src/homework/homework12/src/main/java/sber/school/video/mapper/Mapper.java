package sber.school.video.mapper;

import sber.school.video.dto.GenericDto;
import sber.school.video.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDto> {
  E toEntity(D dto);

  List<E> toEntityList(List<D> dtoList);

  D toDto(E entity);

  List<D> toDtoList(List<E> entityList);
}
