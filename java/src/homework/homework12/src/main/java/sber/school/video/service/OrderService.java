package sber.school.video.service;

import org.springframework.stereotype.Service;
import sber.school.video.dto.OrderDto;
import sber.school.video.dto.OrderFullDto;
import sber.school.video.mapper.OrderFullMapper;
import sber.school.video.mapper.OrderMapper;
import sber.school.video.model.Order;
import sber.school.video.repository.OrderRepository;

import java.util.List;

@Service
public class OrderService extends GenericService<Order, OrderDto> {
    private final OrderRepository repository;
    private final OrderFullMapper orderFullMapper;

    protected OrderService(
        OrderRepository repository,
        OrderMapper mapper,
        OrderFullMapper orderFullMapper
    ) {
        super(repository, mapper);
        this.repository = repository;
        this.orderFullMapper = orderFullMapper;
    }

    public List<OrderFullDto> getUsersOrders()
    {
        return this.orderFullMapper.toDtoList(repository.findAllByOrderByRentDateDesc());
    }

    public List<OrderFullDto> getUserOrders(Long userId)
    {
        return this.orderFullMapper.toDtoList(repository.findAllByUserIdOrderByRentDateDesc(userId));
    }
}
