package sber.school.video.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sber.school.video.dto.GenericDto;
import sber.school.video.model.GenericModel;
import sber.school.video.service.GenericService;

import java.util.List;

public abstract class RESTGenericController<T extends GenericModel, N extends GenericDto> {
    private final GenericService<T, N> service;

    public RESTGenericController(GenericService<T, N> service) {
        this.service = service;
    }

    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(
        method = RequestMethod.GET
    )
    public ResponseEntity<List<N>> getAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @Operation(description = "Получить запись по ID", method = "get")
    @RequestMapping(
        value = "/{id}",
        method = RequestMethod.GET
    )
    public ResponseEntity<N> get(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.findOne(id));
    }

    @Operation(description = "Создать новую запись", method = "create")
    @RequestMapping(
        method = RequestMethod.POST
    )
    public ResponseEntity<N> create(@RequestBody N entityDto) {
        if (entityDto.getId() != null && entityDto.getId() != 0) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity
            .status(HttpStatus.OK)
            .body(service.create(entityDto));
    }

    @Operation(description = "Удалить запись по ID", method = "delete")
    @RequestMapping(
        value = "/{id}",
        method = RequestMethod.DELETE
    )
    public void delete(@PathVariable("id") Long id) {
        service.deleteById(id);
    }

    @Operation(description = "Обновить запись", method = "update")
    @RequestMapping(
        value = "/{id}",
        method = RequestMethod.PUT
    )
    public ResponseEntity<N> update(@PathVariable(value = "id") Long id, @RequestBody N entityDto) {
        if (!service.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        entityDto.setId(id);

        return ResponseEntity.ok(service.update(entityDto));
    }
}
