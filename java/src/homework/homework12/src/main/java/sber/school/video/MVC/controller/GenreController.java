package sber.school.video.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sber.school.video.dto.GenreDto;
import sber.school.video.service.GenreService;

@Controller
@RequestMapping("/genre")
public class GenreController {
    private final GenreService genreService;

    public GenreController(
        GenreService genreService
    ) {
        this.genreService = genreService;
    }

    @GetMapping("/list")
    public String getAll(Model model) {
        model.addAttribute("genres", genreService.findAll());
        return "genre/list";
    }

    @GetMapping(value = {"/form", "/form/{id}"})
    public String form(Model model, @PathVariable(required = false) Long id) {
        model.addAttribute("genre", id == null ? new GenreDto() : genreService.findOne(id));

        return "genre/form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("genreForm") GenreDto genreDto) {
        if (genreDto.getId() == null) {
            genreService.create(genreDto);
        } else {
            genreService.update(genreDto);
        }

        return "redirect:/genre/list";
    }
}
