package sber.school.video.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;
import sber.school.video.dto.DirectorDto;
import sber.school.video.model.Director;
import sber.school.video.service.DirectorService;

@RestController
@RequestMapping("/rest/directors")
@Tag(name = "Авторы", description = "Контроллер для работы с авторами")
@SecurityRequirement(name = "Bearer Authentication")
public class RESTDirectorController extends RESTGenericController<Director, DirectorDto> {
    private final DirectorService service;

    public RESTDirectorController(DirectorService service) {
        super(service);
        this.service = service;
    }

    @Operation(description = "Добавить режиссеру фильм", method = "addFilm")
    @RequestMapping(
        value = "add-film/director/{directorId}/film/{filmId}",
        method = RequestMethod.PUT
    )
    public ResponseEntity<DirectorDto> addFilm(
        @PathVariable(value = "directorId") Long directorId,
        @PathVariable(value = "filmId") Long filmId
    ) {
        try {
            return ResponseEntity.ok(service.addFilm(directorId, filmId));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
