package sber.school.video.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDto extends GenericDto {
    private String fullName;
    private String position;
    private Set<Long> filmsId;
}
