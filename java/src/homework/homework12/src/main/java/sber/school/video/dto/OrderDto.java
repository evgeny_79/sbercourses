package sber.school.video.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sber.school.video.model.Film;
import sber.school.video.model.User;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto extends GenericDto {
    private LocalDate rentDate;
    private Integer rentPeriod;
    private Boolean isPurchased;
    private Long userId;
    private Long filmId;
}
