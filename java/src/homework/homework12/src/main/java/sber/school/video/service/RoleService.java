package sber.school.video.service;

import org.springframework.stereotype.Service;
import sber.school.video.dto.RoleDto;
import sber.school.video.mapper.RoleMapper;
import sber.school.video.model.Role;
import sber.school.video.repository.RoleRepository;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RoleService extends GenericService<Role, RoleDto> {
    private final RoleRepository repository;

    protected RoleService(
        RoleRepository repository,
        RoleMapper mapper
    ) {
        super(repository, mapper);
        this.repository = repository;
    }

    public Map<Long, String> getKeyValueList() {
        return repository.findAll().stream().collect(Collectors.toMap(
            Role::getId, Role::getTitle
        ));
    }

    public Long findRoleIdByTitle(String title) {
        Role role = repository.findByTitle(title);
        return role.getId();
    }
}
