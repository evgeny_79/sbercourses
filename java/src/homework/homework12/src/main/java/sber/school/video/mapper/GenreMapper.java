package sber.school.video.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.video.dto.GenreDto;
import sber.school.video.model.Genre;

@Component
public class GenreMapper extends GenericMapper<Genre, GenreDto> {
    protected GenreMapper(ModelMapper modelMapper) {
        super(modelMapper, Genre.class, GenreDto.class);
    }
}
