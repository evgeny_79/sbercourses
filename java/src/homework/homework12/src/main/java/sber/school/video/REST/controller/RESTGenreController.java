package sber.school.video.REST.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sber.school.video.dto.GenreDto;
import sber.school.video.model.Genre;
import sber.school.video.service.GenreService;

@RestController
@RequestMapping("/rest/genres")
@Tag(name = "Жанры", description = "Контроллер для работы с жанрами")
@SecurityRequirement(name = "Bearer Authentication")
public class RESTGenreController extends RESTGenericController<Genre, GenreDto> {
    public RESTGenreController(GenreService service) {
        super(service);
    }
}
