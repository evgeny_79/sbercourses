package sber.school.video.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
public class GenericModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen")
    @Column(name = "id", nullable = false)
    protected Long id;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    @Column(name = "created_when", updatable = false)
    protected LocalDateTime createdWhen;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    @Column(name = "created_by", updatable = false)
    protected String createdBy;

    @PrePersist
    protected void prePersist() {
        if (createdWhen == null) {
            createdWhen = LocalDateTime.now();
        }

        if (StringUtils.isBlank(createdBy)) {
            createdBy = "DEFAULT_USER";
        }
    }
}
