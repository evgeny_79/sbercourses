package sber.school.video.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import sber.school.video.dto.FilmDto;
import sber.school.video.dto.FilmPromoDto;
import sber.school.video.mapper.FilmMapper;
import sber.school.video.mapper.FilmPromoMapper;
import sber.school.video.model.Director;
import sber.school.video.model.Film;
import sber.school.video.repository.DirectorRepository;
import sber.school.video.repository.FilmRepository;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDto> {
    private final FilmRepository repository;
    private final FilmMapper mapper;
    private final FilmPromoMapper promoMapper;
    private final DirectorRepository directorRepository;

    protected FilmService(
        FilmRepository repository,
        FilmMapper filmMapper,
        FilmPromoMapper promoMapper,
        DirectorRepository directorRepository
    ) {
        super(repository, filmMapper);
        this.repository = repository;
        this.mapper = filmMapper;
        this.promoMapper = promoMapper;
        this.directorRepository = directorRepository;
    }

    public FilmDto addDirector(Long filmId, Long directorId) {
        Director director = directorRepository
            .findById(directorId)
            .orElseThrow(() -> new NotFoundException("Режиссер не найден"));

        Film film = repository
            .findById(filmId)
            .orElseThrow(() -> new NotFoundException("Фильм не найден"));

        film.getDirectors().add(director);

        return mapper.toDto(repository.save(film));
    }

    public List<FilmPromoDto> getPromoList() {
        return promoMapper.toDtoList(repository.findAll());
    }
}
