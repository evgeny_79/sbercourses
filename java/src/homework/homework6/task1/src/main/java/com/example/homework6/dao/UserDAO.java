package com.example.homework6.dao;

import com.example.homework6.models.Book;
import com.example.homework6.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // добавляем пользователя в БД
    public void save(User user){
        jdbcTemplate.update("INSERT INTO public.users(lastName, firstname, birthdate," +
                "phone, email) VALUES(?, ?, ?, ?, ?)", user.getLastName(), user.getFirstName(),
                user.getBirthDate(), user.getPhone(), user.getEmail());
    }

    // получить пользователя по его телефону
    public User getUserByPhone(String phone){
        return jdbcTemplate.query("SELECT * FROM public.users WHERE phone=?", new Object[] {phone},
                new BeanPropertyRowMapper<>(User.class)).stream().findAny().orElse(null);
    }

    // получить список книг пользователя
    public List<Book> getUserBooks(String phone){
        User user = getUserByPhone(phone);
        return jdbcTemplate.query("SELECT * FROM public.books b INNER JOIN public.users_books ub ON ub.book_id=b.id WHERE ub.user_id=?",
                new Object[]{user.getId()}, new BeanPropertyRowMapper<>(Book.class));
    }

    // добавить книгу пользователю
    public void addBookToUser(String phone, int bookID){
        User user = getUserByPhone(phone);
        jdbcTemplate.update("INSERT INTO public.users_books VALUES (?, ?)", user.getId(), bookID);
    }





}
