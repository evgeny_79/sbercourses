package com.example.homework6.dao;

import com.example.homework6.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BookDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Book> index() {
        return jdbcTemplate.query("SELECT * FROM books", new BeanPropertyRowMapper<>(Book.class));
    }

    // добавить книгу в БД books
    public void save(Book book) {
        jdbcTemplate.update("INSERT INTO public.books(title, author, publication_year) VALUES(?, ?, ?)",
                book.getTitle(), book.getAuthor(), book.getPublicationYear());
    }

}
