package com.example.alishev_project1;

import com.example.homework6.dao.BookDAO;
import com.example.homework6.dao.UserDAO;
import com.example.homework6.models.Book;
import com.example.homework6.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class Homework6Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Homework6Application.class);

    private final BookDAO bookDAO;
    private final UserDAO userDAO;

    @Autowired
    public Homework6Application(BookDAO bookDAO, UserDAO userDAO) {
        this.bookDAO = bookDAO;
        this.userDAO = userDAO;
    }


    public static void main(String[] args) {
        SpringApplication.run(Homework6Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
		
		// добавить книгу пользователю
        userDAO.addBookToUser("622-22-22", 8);
		
		// получить книги пользователя
        List<Book> books = userDAO.getUserBooks("622-22-22");
        books.forEach(e -> log.info(e.toString()));

        // добавим книгу в БД
        Book book = new Book(9, "Beginning Hibernate 6", "Joseph B.Ottinger", 2022);
        bookDAO.save(book);

        // добавим пользователя в БД
        User user = new User("Либерзон", "Иннокентий", LocalDate.of(1995, 06, 10), "622-55-55", "liberzon@mail.ru");
        userDAO.save(user);

        
    }
}
