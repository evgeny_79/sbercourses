create table public.users
(
    id         serial primary key,
    lastName   varchar(30) not null,
    firstName  varchar(30) not null,
    birthDate  date        not null,
    phone      varchar(30) not null,
    email      varchar(30) not null
);

insert into public.users(lastname, firstname, birthdate, phone, email)
VALUES ('Иванов', 'Иван', '1990-01-01', '622-22-22', 'ivan@mail.ru'),
       ('Петров', 'Петр', '2000-12-12', '622-33-33', 'petr@mail.ru'),
       ('Сидоров', 'Сергей', '1995-06-06', '622-44-44', 'sidor@mail.ru');