create table public.books
(
    id         serial primary key,
    title      varchar(100) not null,
    author     varchar(100) not null,
    publication_year int    not null
);

insert into public.books(title, author, publication_year)
VALUES ('Изучаем SQL', 'Линн Бейли', 2012),
       ('SQL. Сборник рецептов', 'Энтони Молинаро', 2009),
       ('SQL', 'Крис Фиайли', 2013),
       ('SQL. Библия пользователя', 'Алекс Кригель', 2010),
       ('Искусство статистики. Как находить ответы в данных', 'Дэвид Шпигельхалтер', 2020),
       ('Data Science. Наука о данных с нуля', 'Джоэл Грас', 2017),
       ('Основы Data Science и Big Data. Python и наука о данных','Дэви Силен', 2017);
