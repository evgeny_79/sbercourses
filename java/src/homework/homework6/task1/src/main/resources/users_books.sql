create table public.users_books
(
    user_id    INT REFERENCES users (id),
    book_id  INT REFERENCES books (id)
);