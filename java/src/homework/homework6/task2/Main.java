package src.homework.homework6.task2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        isArmstrongNumber();
    }

    private static void isArmstrongNumber() {
        System.out.println("Проверим, является ли введенное число - числом Армстронга");

        String hint = """
            Число Армстронга — натуральное число.
            Натуральные числа: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 и т.д.
            """;

        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                Long n;

                System.out.print("Введите число: ");

                if (sc.hasNextLong()) {
                    long value = sc.nextLong();
                    if (value > 0) {
                        n = value;
                    } else {
                        System.out.println(hint);
                        continue;
                    }
                } else {
                    System.out.println(hint);
                    sc.next();
                    continue;
                }

                Long sum = 0L;
                int power = n.toString().length();

                long m = n;
                while (m > 0) {
                    long number = m % 10;
                    sum += (long) Math.pow(number, power);
                    m /= 10;
                }

                System.out.println(n.equals(sum));
            }
        }
    }
}
