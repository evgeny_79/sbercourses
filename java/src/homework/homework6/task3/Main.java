package src.homework.homework6.task3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        isSimpleNumber();
    }

    private static void isSimpleNumber() {
        System.out.println("Проверим, является ли введенное число - простым числом");

        String hint = """
            Простое число — натуральное число, имеющее ровно два различных натуральных делителя.
            Натуральные числа: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 и т.д.
            """;

        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                long n;

                System.out.print("Введите число: ");

                if (sc.hasNextLong()) {
                    long value = sc.nextLong();
                    if (value > 0) {
                        n = value;
                    } else {
                        System.out.println(hint);
                        continue;
                    }
                } else {
                    System.out.println(hint);
                    sc.next();
                    continue;
                }

                boolean result = true;

                if (n == 1) {
                    result = false;
                }

                for (int i = 2; i < n; ++i) {
                    if (n % i == 0) {
                        result = false;
                        break;
                    }
                }

                System.out.println(result);
            }
        }
    }
}
