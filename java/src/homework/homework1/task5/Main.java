package src.homework.homework1.task5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
         try {
            int n = inputN();
            System.out.println("Успешный ввод!");
        } catch (InputMismatchException ex) {
             ex.printStackTrace();
         }
    }
    private static int inputN() {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n >= 100 || n <= 0) {
            throw new InputMismatchException("Неверный ввод");
        }
        return n;
    }
}
