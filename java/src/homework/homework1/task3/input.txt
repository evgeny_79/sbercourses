Listing 12.16 gives the program. The program checks the number of arguments passed to
the main method (lines 7–11), checks whether the source and target files exist (lines 14–25),
creates a Scanner for the source file (line 29), creates a PrintWriter for the target file
(line 30), and repeatedly reads a line from the source file (line 33), replaces the text (line 34),
and writes a new line to the target file (line 35).