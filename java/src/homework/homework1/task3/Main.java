package src.homework.homework1.task3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try ( FileReader reader = new FileReader("C:/Users/1/IdeaProjects/sbercourses/java/src/homework/task3/input.txt");
              FileWriter writer = new FileWriter("C:/Users/1/IdeaProjects/sbercourses/java/src/homework/task3/output.txt", false)
        ){  int symbol;
            char ch;
            while ((symbol = reader.read())!=-1) {
                ch = (symbol >= 97 && symbol <= 122) ? (char) (symbol - 32): (char) symbol;
                writer.append(ch);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
