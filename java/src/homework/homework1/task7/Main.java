package src.homework.homework1.task7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int largest = 0;
        int secondLargest = 0;
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
            if (array[i] < largest && array[i] > secondLargest)
                secondLargest = array[i];
            if (array[i] >= largest) {
                secondLargest = largest;
                largest = array[i];
            }
        }
        System.out.printf("%d %d", largest, secondLargest);
    }
}
