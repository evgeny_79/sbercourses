package src.homework.homework1.task4;

public class MyEvenNumber {
    private final int n;

    public MyEvenNumber(int n) {
        if (isEven(n)) {
            this.n = n;
        } else {
            throw new IllegalArgumentException("Недопустимое значение " + n);
        }
    }

    private static boolean isEven(int n) {
        return n % 2 == 0;
    }

    public int getN() {
        return n;
    }
}
