package src.homework.homework1.task2;

public class Main {
    public static void main(String[] args) {
        try {
            double result = division(10, 0);
            System.out.println(result);
        } catch (MyUncheckedException e) {
            e.printStackTrace();
        }
    }

    private static double division(double a, double b) throws MyUncheckedException {
        if (b == 0) {
            throw new MyUncheckedException("На ноль делить нельзя");
        }
        return a / b;
    }
}
