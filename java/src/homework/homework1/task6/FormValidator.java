package src.homework.homework1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class FormValidator {
    public static void checkName(String str){
        boolean isValid = str.matches("^[A-ZА-Я][a-zа-яё]{1,19}");
        if (!isValid) {
            throw new IllegalArgumentException("Длина имени должна быть от 2 до 20 символов, первая буква заглавная.");
        }
    }
    public static void checkBirthdate(String str){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date;
        try {
            date = LocalDate.parse(str, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Неправильный формат даты.");
        }
        LocalDate minDate = LocalDate.of(1900, 1, 1);
        LocalDate maxDate = LocalDate.now();
        if (date.isBefore(minDate) || date.isAfter(maxDate)) {
            throw new IllegalArgumentException("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.");
        }
    }
    public static void checkGender(String str){
        try {
            Gender.valueOf(str);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Недопустимое значение: " + str);
        }
    }
    public static void checkHeight(String str) {
        double height;

        try {
            height = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Недопустимое значение: " + str);
        }

        if (height <= 0) {
            throw new IllegalArgumentException("Рост должен быть положительным числом");
        }
    }
}
