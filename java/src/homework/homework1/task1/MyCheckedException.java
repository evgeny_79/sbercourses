package src.homework.homework1.task1;

import java.io.IOException;

public class MyCheckedException extends IOException {
    public MyCheckedException(String message) {
        super(message);
    }
}
