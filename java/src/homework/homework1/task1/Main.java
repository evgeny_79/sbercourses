package src.homework.homework1.task1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        try {
            checkedException();
        } catch (MyCheckedException e) {
            e.printStackTrace();
        }
    }

    private static void checkedException() throws MyCheckedException {
        File file = new File("not_existing_file.txt");
        try {
            FileInputStream stream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new MyCheckedException(String.format("Файл '%s' не найден", file.getName()));
        }
    }
}
