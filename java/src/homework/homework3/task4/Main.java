package src.homework.homework3.task4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Class<?>> set = getAllInterfaces(Demo2.class);
        set.forEach(s -> System.out.println(s.getSimpleName()));


    }
    private static Set<Class<?>> getAllInterfaces(Class<?> clazz){
        Set<Class<?>> set = new HashSet<>();
        Class<?> currentClass = clazz;
        while (currentClass != null) {
            List<Class<?>> i = Arrays.asList(currentClass.getInterfaces());
            set.addAll(i);

            for (Class<?> c : i) {
                set.addAll(getAllInterfaces(c));
            }

            currentClass = currentClass.getSuperclass();
        }

        return set;
    }
}
