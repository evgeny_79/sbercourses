package src.homework.homework3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();
        try {
            Method method = aPrinter.getClass().getDeclaredMethod("print", int.class);
            method.invoke(aPrinter, 999.0);

        } catch (NoSuchMethodException e) {
            System.out.println("Метод не найден.");;
        } catch (InvocationTargetException e) {
            System.out.println("Метод сгенерировал исключение.");;
        } catch (IllegalAccessException e) {
            System.out.println("Нет доступа.");;
        } catch (IllegalArgumentException e) {
            System.out.println("Некорректный аргумент.");
        }
    }
}
