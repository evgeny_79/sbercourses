package src.homework.homework3.task2;

public class Main {
    public static void main(String[] args) {
        printAnno(A.class);
        printAnno(B.class);

    }
    private static void printAnno(Class<?> c){
        boolean isExist = c.isAnnotationPresent(IsLike.class);
        if (isExist){
            IsLike isLike = c.getAnnotation(IsLike.class);
            System.out.println(isLike.value());
        }

    }
}
