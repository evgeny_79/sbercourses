package src.homework.homework3.task6;

public class Main {
    public static void main(String[] args) {
        System.out.println(bracketChecker("{()[]()}"));
        System.out.println(bracketChecker("{)(}"));
        System.out.println(bracketChecker("[}"));
        System.out.println(bracketChecker("[{(){}}][()]{}"));
        System.out.println(bracketChecker(")()"));
        System.out.println(bracketChecker("(qwerty)"));

    }
    private static boolean bracketChecker(String str){
        // создание стека символов
        char[] stackArray = new char[str.length()];
        // вершина стека
        int top = -1;
        for (int i = 0; i < str.length(); i++){
            char ch = str.charAt(i);
            switch (ch){
                case '(', '[', '{':
                    // поместить в стек
                    stackArray[++top] = ch;
                    break;
                case ')', ']', '}':
                    if (top != -1){
                        // извлечь из стека
                        char temp = stackArray[top];
                        if ((ch == ')' && temp == '(') ||
                                (ch == ']' && temp == '[') ||
                                (ch == '}' && temp == '{'))
                            top--;
                    }
                    // строка начинается с закрывающего элемента
                    else
                        return false;
                    break;
            }
        }
        return top == -1;

    }
}
