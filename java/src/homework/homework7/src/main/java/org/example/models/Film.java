package org.example.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Films")
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "premier_year")
    private LocalDate premierYear;

    @Column(name = "country")
    private String country;

    @Enumerated(EnumType.STRING)
    private Genre genre;

    /* по умолчанию Lazy */
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "film_director",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "director_id")
    )
    private List<Director> directors;
}
