package org.example.models;

public enum Genre {

    ACTION,
    ADVENTURE,
    COMEDY,
    DRAMA,
    FANTASY,
    WESTERN;
}
