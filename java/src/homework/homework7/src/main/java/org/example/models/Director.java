package org.example.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "directors")
public class Director {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "directors_fio")
    private String directorFIO;

    @Column(name = "position")
    private int position;

    /* по умолчанию Lazy */
    @ManyToMany(mappedBy = "directors", cascade = CascadeType.ALL)
    List<Film> films;
}
