package src.homework.homework4.task6;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/* Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>. */
public class Main {
    public static void main(String[] args) {
        System.out.println(changeSets(Set.of(Set.of(0, 1, 2), Set.of(3, 1, 5), Set.of(6, 7, 8))));

    }
    private static Set<Integer> changeSets (Set<Set<Integer>> set) {
        return set.stream().flatMap(Collection::stream).collect(Collectors.toSet());
    }
}
