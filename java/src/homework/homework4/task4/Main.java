package src.homework.homework4.task4;

/*На вход подается список вещественных чисел. Необходимо отсортировать их по
  убыванию.*/

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(sortDescending(List.of(0.1, 1.1, 5.8, 10.0)));

    }
    private static List<Double> sortDescending(List<Double> list){
        return list.stream().sorted((x, y) -> y.compareTo(x)).toList();
    }
}
