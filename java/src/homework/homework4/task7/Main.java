package src.homework.homework4.task7;

/* На вход подается две строки. Необходимо определить, можно ли уравнять
   эти две строки, применив только одну из трех возможных операций:
        1. Добавить символ
        2. Удалить символ
        3. Заменить символ
        Пример:
        “cat” “cats” -> true
        “cat” “cut” -> true
        “cat” “nut” -> false */

public class Main {
    public static void main(String[] args) {
        System.out.println(checker("cat", "cats"));
        System.out.println(checker("cat", "ca"));
        System.out.println(checker("cat", "nut"));
        System.out.println(checker("cat", "tac"));



    }
    private static boolean checker(String s1, String s2) {
        // если длины строк различаются более чем на 1 символ
        if (Math.abs(s1.length() - s2.length()) > 1) {
            return false;
        }
        // находим строку с минимальной длиной
        int minLen = (s1.length() > s2.length()) ? s2.length() : s1.length();
        /* Количество несовпадений в строках. */
        int countOfChange = (s1.length() == s2.length()) ? 0 : 1;
        for (int i = 0; i < minLen; i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                countOfChange++;
            }
            if (countOfChange > 1)
                break;
        }
        if (countOfChange <= 1)
            return true;
        return false;
    }
}
