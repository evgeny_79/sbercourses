package src.homework.homework4.task3;

/* На вход подается список строк. Необходимо вывести количество непустых строк в
   списке. Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.*/

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(count(List.of("abc", "", "", "def", "qqq")));
    }
    private static long count(List<String> list) {
        return list.stream().filter(s -> !s.isEmpty()).count();
    }
}
