package src.homework.homework4.task5;

/*На вход подается список непустых строк. Необходимо привести все символы строк к
  верхнему регистру и вывести их, разделяя запятой. Например, для
   List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.*/

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println(changeString(List.of("abc", "def", "qqq")));

    }
    private static String changeString (List<String> list) {
        return list.stream().map(String::toUpperCase).collect(Collectors.joining(", "));
    }
}
