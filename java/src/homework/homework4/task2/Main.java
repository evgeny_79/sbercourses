package src.homework.homework4.task2;

/*На вход подается список целых чисел. Необходимо вывести результат перемножения
  этих чисел. Например, если на вход передали List.of(1, 2, 3, 4, 5),
  то результатом должно быть число 120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).*/

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(product(List.of(1,2,3,4,5)));

    }
    private static int product(List<Integer> list){
        return list.stream().reduce(1, (x, y) -> x * y);
    }
}
